<?php

namespace Database\Seeders;

use App\Enum\Role\RoleEnum;
use App\Models\Role;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Role::query()->create([
            'id' => 1,
            'name' => RoleEnum::ADMIN->labelEng()
        ]);

        Role::query()->create([
            'id' => 2,
            'name' => RoleEnum::USER->labelEng()
        ]);
    }
}
