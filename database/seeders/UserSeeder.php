<?php

namespace Database\Seeders;

use App\Enum\Role\RoleEnum;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::query()->create([
            'name' => 'admin',
            'email' => 'admin@mail.ru',
            'password' => Hash::make('123456789'),
            'role_id' => RoleEnum::ADMIN->value
        ]);

        /*for ($i = 0; $i < 10; $i++) {
            User::query()->create([
                'name' => fake()->name(),
                'email' => fake()->email(),
                'password' => Hash::make('123456789'),
                'role_id' => RoleEnum::USER->value
            ]);
        }*/
    }
}
