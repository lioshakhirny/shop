<?php

use Illuminate\Support\Facades\Route;

Route::get('/', [\App\Http\Controllers\Site\MainController::class, 'index'])->name('main');
Route::prefix('catalog')
    ->controller(\App\Http\Controllers\Site\CatalogController::class)
    ->name('catalog.')
    ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/{id}', 'show')->name('show');
        Route::get('filter', 'filter')->name('filter');
    });

Route::prefix('login')
    ->controller(\App\Http\Controllers\Auth\LoginController::class)
    ->name('login.')
    ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/store', 'store')->name('store');
    });

Route::prefix('register')
    ->controller(\App\Http\Controllers\Auth\RegisterController::class)
    ->name('register.')
    ->group(function () {
        Route::post('/store', 'store')->name('store');
    });

Route::prefix('user')
    ->name('user.')
    ->middleware('user')
    ->group(function () {
        Route::get('/', [\App\Http\Controllers\User\UserController::class, 'index'])->name('index');
        Route::post('/{id}/update', [\App\Http\Controllers\User\UserController::class, 'update'])->name('update');
        Route::get('/logout', [\App\Http\Controllers\User\UserController::class, 'logout'])->name('logout');
        Route::post('/add-adress', [\App\Http\Controllers\User\UserController::class, 'createAdress'])->name('add-adress');
        Route::post('/review', [\App\Http\Controllers\User\UserController::class, 'review'])->name('review.store');
        Route::delete('/address/{id}', [\App\Http\Controllers\User\UserController::class, 'deleteAdress'])->name('address.delete');
    });

Route::prefix('busket')
    ->name('busket.')
    ->controller(\App\Http\Controllers\Site\BusketController::class)
    ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/add-product', 'addProduct')->name('add-product');
        Route::post('/add-order', 'addOrder')->name('add-order');
        Route::post('delete/order', 'delete')->name('delete-order');
    });

Route::prefix('admin')
    ->name('admin.')
    ->middleware('admin')
    ->group(function () {
       Route::get('/', [\App\Http\Controllers\Admin\AdminController::class, 'index'])->name('index');
       Route::post('/product/store', [\App\Http\Controllers\Admin\ProductController::class, 'store'])->name('product.store');
       Route::get('/logout', [\App\Http\Controllers\Admin\AdminController::class, 'logout'])->name('logout');
       Route::post('/store/shoe-size', [\App\Http\Controllers\Admin\ShoeSizeController::class, 'store'])->name('shoe-size.store');
       Route::post('/store/color', [\App\Http\Controllers\Admin\ColorController::class, 'store'])->name('color.store');
       Route::post('/blog/store', [\App\Http\Controllers\Admin\BlogController::class, 'store'])->name('blog.store');
       Route::delete('/user/{id}', [\App\Http\Controllers\Admin\AdminController::class, 'deleteUser'])->name('user.delete');
       Route::delete('/product/{id}/delete', [\App\Http\Controllers\Admin\ProductController::class, 'delete'])->name('product.delete');
       Route::delete('/delete/review/{id}', [\App\Http\Controllers\Admin\ReviewController::class, 'delete'])->name('review.delete');
       Route::post('/promocode/store', [\App\Http\Controllers\Admin\PromocodeController::class, 'store'])->name('promocode.store');
       Route::delete('/promocode/delete/{id}', [\App\Http\Controllers\Admin\PromocodeController::class, 'delete'])->name('promocode.delete');
       Route::delete('/blog/delete/{id}', [\App\Http\Controllers\Admin\BlogController::class, 'delete'])->name('blog.delete');
       Route::post('answer/{id}', [\App\Http\Controllers\Admin\ContactController::class, 'store'])->name('answer.store');
       Route::patch('/update/blog/{id}', [\App\Http\Controllers\Admin\BlogController::class, 'update'])->name('blog.update');
       Route::put('/order/update/{id}', [\App\Http\Controllers\Admin\OrderController::class, 'update'])->name('order.update');
       Route::put('/product/{id}/update', [\App\Http\Controllers\Admin\ProductController::class, 'update'])->name('product.update');
    });

Route::prefix('contact')
    ->name('contact.')
    ->controller(\App\Http\Controllers\Site\ContactController::class)
    ->group(function () {
        Route::get('/', 'index')->name('index');
        Route::post('/store', 'store')->name('store');
    });

Route::view('/information', 'site.information.information')->name('information');
Route::view('/blog', 'site.blog.blog')->name('blog');
Route::get('/blog', [\App\Http\Controllers\BlogController::class, 'index'])->name('blog');
