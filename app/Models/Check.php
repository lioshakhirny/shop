<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Check extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'total_sum',
        'payment_method',
        'status_payment',
        'adress',
        'date'
    ];
}
