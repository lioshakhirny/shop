<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'promocode_id',
        'user_id',
        'count',
        'sum',
        'date',
        'status',
        'time' ,
        'adress_id'
    ];
}
