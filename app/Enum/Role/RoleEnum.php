<?php

namespace App\Enum\Role;

enum RoleEnum: int
{
    case ADMIN = 1;
    case USER = 2;

    public function label(): string
    {
        return match ($this) {
            self::ADMIN => 'Администратор',
            self::USER => 'Пользователь'
        };
    }

    public function labelEng(): string
    {
        return match ($this) {
            self::ADMIN => 'admin',
            self::USER => 'user'
        };
    }
}
