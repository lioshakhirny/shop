<?php

namespace App\Enum\Order;

enum StatusEnum: int
{
    case NEW = 1;
    case DELIVERY = 2;
    case END = 3;

    public function lable (): string
    {
        return match ($this) {
            self::NEW => 'Новый',
            self::DELIVERY => 'В пути',
            self::END => 'Доставлен'
        };
    }
}
