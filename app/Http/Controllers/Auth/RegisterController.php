<?php

namespace App\Http\Controllers\Auth;

use App\Enum\Role\RoleEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
    public function store(RegisterRequest $request)
    {
        if ($request->password == $request->reset_password) {
            $user = User::query()->create([
                'name' => $request->name,
                'email' => $request->email,
                'role_id' => RoleEnum::USER->value,
                'password' => Hash::make($request->password),
                'date_account_create' => Date::now(),
                'created_at' => Date::now()
            ]);

            Auth::loginUsingId($user->id);

            Cache::put('new_user', $user->id);

            return redirect()->route('user.index');
        }
        return redirect()->back()->with('danger', 'Пароли не совпадают');
    }
}
