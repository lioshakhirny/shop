<?php

namespace App\Http\Controllers\Auth;

use App\Enum\Role\RoleEnum;
use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('site.auth.index');
    }

    public function store(LoginRequest $request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            if (Auth::user()->role_id == RoleEnum::ADMIN->value) {
                return redirect()->route('admin.index');
            } elseif (Auth::user()->role_id == RoleEnum::USER->value) {
                return redirect()->route('user.index');
            }
        }
        // Если аутентификация не удалась, возвращаемся обратно с ошибкой
        return redirect()->back()
            ->withErrors(['email' => 'Неправильный email или пароль'])
            ->withInput();
    }
}
