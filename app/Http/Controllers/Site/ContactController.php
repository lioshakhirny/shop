<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function index (): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('site.kontakts.kontakts');
    }

    public function store (ContactRequest $request): \Illuminate\Http\RedirectResponse
    {
        Contact::query()->create([
            'name' => $request->name,
            'email' => $request->email,
            'description' => $request->description,
            'phone' => $request->phone
        ]);

        return redirect()->back()->with('success', 'Спасибо что оставили ваши контакты');
    }
}
