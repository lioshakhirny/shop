<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ShoeSize;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;

class CatalogController extends Controller
{
    public function index(Request $request): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $products = Product::query()
            ->when(!is_null($request->product_id), fn(Builder $builder) => $builder->whereIn('id', $request->product_id))
            ->when(!is_null($request->min) && !is_null($request->max), fn(Builder $builder) => $builder->whereBetween('price', [$request->min, $request->max]))
            ->when(!is_null($request->sizes), function (Builder $builder) use ($request) {
                $builder->join('shoe_sizes', 'shoe_sizes.product_id', '=', 'products.id')
                    ->whereIn('shoe_sizes.size', $request->sizes);
            })
            ->when(!is_null($request->color), function (Builder $builder) use ($request) {
                $builder->join('shoe_colors', 'shoe_colors.product_id', '=', 'products.id')
                    ->whereIn('shoe_colors.color', $request->color);
            })
            ->when(!is_null($request->sort), fn(Builder $builder) => $builder->orderBy('price', 'asc'))
            ->select('products.*')
            ->groupBy('products.id')
            ->get();


        if (!empty($request->model)) {
            $products = Product::query()
                ->when(!is_null($request->model), fn(Builder $builder) => $builder->where('model', 'LIKE', '%' . $request->model . '%'))
                ->get();
        }


        $sizes = ShoeSize::query()
            ->orderBy('size')
            ->select('size')
            ->groupBy('size')
            ->get();

        return view('site.catalog.index', [
            'products' => $products ?? Product::all(),
            'sizes' => $sizes
        ]);
    }

    public function show(int $id): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        $product = Product::query()->find($id);

        $images = Storage::allFiles('public/files/products/' . $product->id);
        $products = Product::query()->where('id', '<>', $id)->paginate(3);
        $sizes = ShoeSize::query()->where('product_id', '=', $id)
            ->orderBy('size')
            ->get();

        return view('site.catalog.show', [
            'product' => $product,
            'image_one' => Storage::url(Arr::first($images)),
            'image_two' => Storage::url($images[1] ?? null),
            'image_three' => Storage::url($images[2] ?? null),
            'products' => $products,
            'sizes' => $sizes
        ]);
    }

    public function filter (Request $request)
    {
        dd($request);
    }
}
