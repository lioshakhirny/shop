<?php

namespace App\Http\Controllers\Site;

use App\Enum\Order\StatusEnum;
use App\Http\Controllers\Controller;
use App\Models\Adress;
use App\Models\Contact;
use App\Models\Order;
use App\Models\Product;
use App\Models\Promocode;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Date;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class BusketController extends Controller
{
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('site.busket.index', [
            'carts' => Session::get('carts'),
            'adreses' => Adress::query()->where('user_id', '=', Auth::id())->get()
        ]);
    }

    public function addProduct(Request $request)
    {
        if (auth()->check()) {
            Session::start();
            $carts = Session::get('carts');
            $product = Product::query()->find($request->product_id);
            if ($product->count() != 0) {
                if (isset($carts[$product->id])) {
                    $count = $carts[$product->id]['count'] + 1;
                    $carts[$product->id] = [
                        'product_id' => $product->id,
                        'user_id' => $request->user_id,
                        'brand' => $product->brand,
                        'model' => $product->model,
                        'count' => $count,
                        'price' => $product->price * $count,
                        'description' => $product->description
                    ];
                } else {
                    $carts[$product->id] = [
                        'product_id' => $product->id,
                        'user_id' => $request->user_id,
                        'brand' => $product->brand,
                        'model' => $product->model,
                        'count' => 1,
                        'price' => $product->price,
                        'description' => $product->description
                    ];
                }
                Session::put('carts', $carts);
                Session::save();

                return response()->json([
                    'success' => true,
                    'data' => route('busket.index'),
                ], 200);
            }
        }
        return response()->json([
            'success' => false,
            'data' => 'Для покупки товаров, вам нужно авторизоваться или зарегистрироваться'
        ], 200);
    }

    public function addOrder (Request $request)
    {
        if (empty($request->code)) {
            return  redirect()->back()->with('Промокод пустой');
        }

        $promocode = Promocode::query()->where('code', '=', $request->code)->first();

        if (empty($promocode)) {
            return redirect()->back()->with('Такого промокода не существует');
        }

        foreach (Session::get('carts') as $item) {

            $product = Product::query()->find($item['product_id']);

            if ($product->count >= $item['count']) {
                $product->update([
                    'count' => $product->count - $item['count']
                ]);
            } else {
                return redirect()->back()->with('Нет количества товара');
            }

            Order::query()->create([
                'product_id' => $item['product_id'],
                'promocode_id' => $promocode->id,
                'user_id' => Auth::id(),
                'count' => $item['count'],
                'sum' => $item['price'] - $promocode->discount,
                'time' => Date::now(),
                'status' => StatusEnum::NEW->value,
                'adress_id' => $request->adress_id
            ]);
        }

        Mail::send('admin.verification.busket', ['carts' => Session::get('carts')],
            function ($message) {
                $message->to(\auth()->user()->email, '')->subject
                ('Sneakers');
                $message->from(\auth()->user()->email, 'Sneakers');
            });

        Session::forget('carts');

        return redirect()->route('user.index');
    }

    public function delete (Request $request)
    {
        Session::forget('carts.' . $request->product_id);

        return redirect()->back();
    }
}
