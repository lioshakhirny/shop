<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index ()
    {
        return view('site.blog.blog', [
            'blogs' => Blog::all()
        ]);
    }
}
