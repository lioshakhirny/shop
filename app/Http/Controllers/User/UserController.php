<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use App\Models\Adress;
use App\Models\Order;
use App\Models\Review;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index(): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('user.index', [
            'adress' => Adress::query()->where('user_id', '=', Auth::id())->get(),
            'orders' => Order::query()->where('user_id', '=', Auth::id())->get()
        ]);
    }

    public function update(int $id, UserUpdateRequest $request)
    {
        User::query()->find($id)->update($request->validated());

        return redirect()->route('user.index')->with('success', 'Вы успешно обновили свой профиль');
    }

    public function logout(): \Illuminate\Http\RedirectResponse
    {
        Auth::logout();

        return redirect()->route('main');
    }

    public function createAdress (Request $request)
    {
       Adress::query()->create([
           'country' => $request->country,
           'city' => $request->city,
           'house' => $request->house,
           'room' => $request->room,
           'user_id' => Auth::id()
       ]);

        return redirect()->back();
    }


    public function delete (User $id)
    {
        $id->delete();

        return redirect()->back();
    }

    public function review (Request $request)
    {
        Review::query()->create([
            'name' => $request->name,
            'description' => $request->description,
            'created_at' => $request->date
        ]);

        return redirect()->back();
    }

    public function deleteAdress (Adress $id): \Illuminate\Http\RedirectResponse
    {
        $id->delete();

        return redirect()->back();
    }
}
