<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Models\Order;
use App\Models\Product;
use App\Models\ShoeColor;
use App\Models\ShoeSize;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    public function store(ProductRequest $request)
    {
        $product = Product::query()->create([
            'brand' => $request->brand,
            'model' => $request->model,
            'price' => $request->price,
            'count' => $request->count,
            'description' => $request->description
        ]);

        foreach ($request->images as $image) {
            $newFileName = $image->getClientOriginalName() . '.' . $image->getClientOriginalExtension();
            Storage::putFileAs('public/files/products/' . $product->id, $image, $newFileName);
        }

        return redirect()->route('admin.index');
    }

    public function update(Product $id, Request $request)
    {
        $id->update([
            'brand' => $request->brand,
            'model' => $request->model,
            'price' => $request->price,
            'count' => $request->count,
            'description' => $request->description
        ]);

        Storage::deleteDirectory('public/files/products/' . $id->id);

        foreach ($request->images as $image) {
            $newFileName = $image->getClientOriginalName() . '.' . $image->getClientOriginalExtension();
            Storage::putFileAs('public/files/products/' . $id->id, $image, $newFileName);
        }

        return redirect()->route('admin.index');
    }

    public function delete(Product $id)
    {
        Order::query()->where(['product_id' => $id->id ])->delete();
        ShoeColor::query()->where(['product_id' => $id->id ])->delete();
        ShoeSize::query()->where(['product_id' => $id->id ])->delete();

        $id->delete();

        return redirect()->back();
    }
}
