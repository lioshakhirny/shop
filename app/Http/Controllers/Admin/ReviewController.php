<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function delete (Review $id)
    {

        $id->delete();

        return redirect()->route('admin.index');
    }
}
