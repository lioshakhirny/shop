<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ShoeSize;
use Illuminate\Http\Request;

class ShoeSizeController extends Controller
{
    public function store (Request $request)
    {
        foreach ($request->size as $size) {
            ShoeSize::query()->create([
                'product_id' => $request->product_id,
                'size' => $size
            ]);
        }

        return redirect()->back();
    }
}
