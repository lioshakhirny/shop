<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Contact;
use App\Models\Order;
use App\Models\Product;
use App\Models\Promocode;
use App\Models\Review;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin.index',[
            'users' => User::query()->where('role_id', '=', 2)->get(),
            'products' => Product::query()->paginate(15),
            'product_count' => Product::query()->count(),
            'newUser' => User::query()->find(Cache::get('new_user')),
            'orders' => Order::query()->get(),
            'reviews' => Review::all(),
            'promocodes' => Promocode::all(),
            'blogs' => Blog::all(),
            'contacts' => Contact::all()
        ]);
    }

    public function logout(): \Illuminate\Http\RedirectResponse
    {
        Auth::logout();

        return redirect()->route('main');
    }

    public function deleteUser (User $id) {

        $id->delete();

        return redirect()->back();
    }
}
