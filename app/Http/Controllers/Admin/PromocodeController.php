<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Promocode;
use App\Models\Review;
use Illuminate\Http\Request;

class PromocodeController extends Controller
{
    public function store (Request $request)
    {
        Promocode::query()->create([
            'code' => $request->code,
            'discount' => $request->discount,
            'disable_date' => now()
        ]);

        return redirect()->route('admin.index');
    }

    public function delete (Promocode $id)
    {
        Order::query()
            ->where('promocode_id', '=', $id->id)
            ->delete();

        $id->delete();

        return redirect()->route('admin.index');
    }
}
