<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function store (Request $request)
    {
        Blog::query()->create([
            'title' => $request->title,
            'description' => $request->description,
            'created_at' => $request->date
        ]);

        return redirect()->route('admin.index');
    }

    public function delete (Blog $id)
    {

        $id->delete();

        return redirect()->route('admin.index');
    }

    public function update (Blog $id, Request $request)
    {
        $id->update([
            'title' => $request->title,
            'description' => $request->description,
            'created_at' => $request->date
        ]);

        return redirect()->route('admin.index');
    }
}
