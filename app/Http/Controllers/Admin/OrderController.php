<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function update (Order $id, Request $request)
    {
        $id->update([
            'status' => $request->status
        ]);


        return redirect()->route('admin.index');
    }
}
