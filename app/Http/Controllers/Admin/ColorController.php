<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ShoeColor;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function store (Request $request)
    {
        foreach ($request->color as $color) {
            ShoeColor::query()->create([
                'product_id' => $request->product_id,
                'color' => $color
            ]);
        }

        return redirect()->back();
    }
}
