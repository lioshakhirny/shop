<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function store (int $id, Request $request)
    {
        Mail::send('admin.verification.question', ['question' =>  Contact::find($id)->description,
            'otvet' => $request->answer], function ($message) use ($id) {
            $message->to(Contact::find($id)->email, '')->subject
            ('Sneakers');
            $message->from('vladstrok23@gmail.com', 'Sneakers');
        });

        Contact::query()->find($id)->delete();

        return redirect()->route('admin.index');
    }
}
