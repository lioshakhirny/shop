/*
document.getElementById('zakaz').addEventListener("click", function () {
    let popupContainer = document.querySelector(".popup-container");


    popupContainer.style.display='block';
});
let closeBtn = document.querySelector(".close-btn");
closeBtn.addEventListener("click", function () {
    let popupContainer = document.querySelector(".popup-container");


    popupContainer.style.display='none';
});
document.addEventListener("DOMContentLoaded", function () {
    var printButton = document.getElementById("pehat");
    printButton.addEventListener("click", function () {
        window.print();
    });
});
*/



document.addEventListener('DOMContentLoaded', function() {
    const checkoutButton = document.querySelector('.checkout-btn');
    const modal = document.getElementById('custom-modal');
    const closeModal = document.querySelector('.custom-close');
    const totalAmountElement = document.getElementById('custom-total-amount').querySelector('span');
    const orderTimeElement = document.getElementById('custom-order-time').querySelector('span');
    const confirmOrderButton = document.getElementById('custom-confirm-order');
    const quantityInput = document.getElementById('custom-quantity');

    checkoutButton.addEventListener('click', function() {
        // Открыть модальное окно
        modal.style.display = 'flex';
        // Установить время оформления заказа
        const now = new Date();
        orderTimeElement.textContent = now.toLocaleString();
    });

    closeModal.addEventListener('click', function() {
        modal.style.display = 'none';
    });

    window.addEventListener('click', function(event) {
        if (event.target == modal) {
            modal.style.display = 'none';
        }
    });


    confirmOrderButton.addEventListener('click', function() {
        alert('Заказ подтвержден!');
        modal.style.display = 'none';
    });
});
