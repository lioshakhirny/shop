let n36 = document.getElementById('36');
let ziz = 'Размер-'

function changeColor(elm) {
    document.querySelector('.size').innerHTML = ziz + elm;
}
document.querySelector('.up').addEventListener('click',
    function() {
        document.getElementById('glav').src =  document.getElementById('up').src
    });
document.querySelector('.sideways').addEventListener('click',
    function() {
        document.getElementById('glav').src = document.getElementById('sideeways').src
    });
document.querySelector('.lap').addEventListener('click',
    function() {
        document.getElementById('glav').src =  document.getElementById('lap').src;
    });
document.getElementById('add').addEventListener('click', function () {
    let kol = document.getElementById('kolvo').value;
    let kol_sklad = document.getElementById('kolvo-sklad').innerText; // Получаем текстовое содержимое
    if (kol <= kol_sklad) {
        $.ajax({
            url: $('.route').val(),         /* Куда пойдет запрос */
            method: 'post',             /* Метод передачи (post или get) */
            data: {
                product_id:$('.product_id').val(),
                size:$('.size').val(),
                user_id:$('.user_id').val(),
                _token: $('meta[name="csrf-token"]').attr('content')
            },     /* Параметры передаваемые в запросе. */
            success: function(data){
                if (!data['success']) {
                    alert(data['data'])
                } else {
                    window.location.assign(data['data'])
                }
            }
        });
    }
    else{
        alert('Превышено число на складе');
    }

})

let price = parseFloat(document.getElementById('price').innerText); // Преобразуем строку в число
let kol = document.getElementById('kolvo'); // Получаем ссылку на элемент input

kol.addEventListener('input', function () {
    let quantity = parseInt(kol.value);

    // Устанавливаем новую цену
    let newPrice = quantity * price; // Умножаем цену на количество
    document.getElementById('ob-summa').textContent = newPrice + 'р'; // Обновляем общую сумму
});

document.querySelector(".txt-line-opisania").addEventListener('click',function (){
    document.querySelector(".container-detali").style.display="none";
    document.querySelector(".container-opisanie").style.display="flex";
})
document.querySelector(".txt-line-detali").addEventListener('click',function (){
    document.querySelector(".container-detali").style.display="flex";
    document.querySelector(".container-opisanie").style.display="none";
})
