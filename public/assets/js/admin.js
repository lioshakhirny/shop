const body = document.querySelector("body"),
    modeToggle = body.querySelector(".mode-toggle");
sidebar = body.querySelector("nav");
sidebarToggle = body.querySelector(".sidebar-toggle");

let getMode = localStorage.getItem("mode");
if(getMode && getMode ==="dark"){
    body.classList.toggle("dark");
}

let getStatus = localStorage.getItem("status");
if(getStatus && getStatus ==="close"){
    sidebar.classList.toggle("close");
}

modeToggle.addEventListener("click", () =>{
    body.classList.toggle("dark");
    if(body.classList.contains("dark")){
        localStorage.setItem("mode", "dark");
    }else{
        localStorage.setItem("mode", "light");
    }
});

sidebarToggle.addEventListener("click", () => {
    sidebar.classList.toggle("close");
    if(sidebar.classList.contains("close")){
        localStorage.setItem("status", "close");
    }else{
        localStorage.setItem("status", "open");
    }
})

const addTovar = document.getElementById('addTovar');
const addContainer = document.getElementById('addContainer');
const Glavna= document.querySelector('.gl');
const Tovars= document.querySelector('.tv');
const Polzovatel= document.querySelector('.pl');
const Zakaz= document.querySelector('.zk');
const size= document.querySelector('.size-snek');
const color= document.querySelector('.color-menu');
const blog = document.querySelector('.bl');
const vopros = document.querySelector('.vp');
const promocod = document.querySelector('.promocod');
const otzv = document.querySelector('.otz');

const ContainerTovars = document.querySelector('.tovars')
const ContainerGlavna = document.querySelector('.dash-content')
const ContainerPolzovatel = document.querySelector('.usser')
const ContainerZakaz = document.querySelector('.zakazz')
const ContainerSize = document.querySelector('.size-sneakers')
const ContainerColor = document.querySelector('.color-sneaker')
const ContainerBlogs = document.querySelector('.containerr-blogs')
const ContainerVopros = document.querySelector('.container-vopros')
const ContainerPromocod = document.querySelector('.container-promokod')
const ContainerOtzvs = document.querySelector('.contaiiner-otzv')


addTovar.addEventListener('click',() => {
    addContainer.style.display="flex";
})
document.querySelector('.close').addEventListener('click',() =>{
    addContainer.style.display="none";

})
/*document.querySelector('.closered').addEventListener('click',() =>{
    document.getElementById('redContainer').style.display="none";

})*/


Glavna.addEventListener('click', () =>{
    ContainerGlavna.style.display="block";
    ContainerTovars.style.display="none";
    ContainerPolzovatel.style.display="none";
    ContainerZakaz.style.display="none";
    ContainerSize.style.display="none";
    ContainerColor.style.display="none";
    ContainerBlogs.style.display="none";
    ContainerVopros.style.display="none";
    ContainerPromocod.style.display="none";
    ContainerOtzvs.style.display="none";
})

Tovars.addEventListener('click', () =>{
    ContainerGlavna.style.display="none";
    ContainerTovars.style.display="flex";
    ContainerPolzovatel.style.display="none";
    ContainerZakaz.style.display="none";
    ContainerSize.style.display="none";
    ContainerColor.style.display="none";
    ContainerBlogs.style.display="none";
    ContainerVopros.style.display="none";
    ContainerPromocod.style.display="none";
    ContainerOtzvs.style.display="none";
})
Polzovatel.addEventListener('click', () =>{
    ContainerGlavna.style.display="none";
    ContainerTovars.style.display="none";
    ContainerPolzovatel.style.display="flex";
    ContainerZakaz.style.display="none";
    ContainerSize.style.display="none";
    ContainerColor.style.display="none";
    ContainerBlogs.style.display="none";
    ContainerVopros.style.display="none";
    ContainerPromocod.style.display="none";
    ContainerOtzvs.style.display="none";
})
Zakaz.addEventListener('click', () =>{
    ContainerGlavna.style.display="none";
    ContainerTovars.style.display="none";
    ContainerPolzovatel.style.display="none";
    ContainerZakaz.style.display="flex";
    ContainerSize.style.display="none";
    ContainerColor.style.display="none";
    ContainerBlogs.style.display="none";
    ContainerVopros.style.display="none";
    ContainerPromocod.style.display="none";
    ContainerOtzvs.style.display="none";
})
size.addEventListener('click', () =>{
    ContainerGlavna.style.display="none";
    ContainerTovars.style.display="none";
    ContainerPolzovatel.style.display="none";
    ContainerZakaz.style.display="none";
    ContainerSize.style.display="flex";
    ContainerColor.style.display="none";
    ContainerBlogs.style.display="none";
    ContainerVopros.style.display="none";
    ContainerPromocod.style.display="none";
    ContainerOtzvs.style.display="none";
})
color.addEventListener('click', () =>{
    ContainerGlavna.style.display="none";
    ContainerTovars.style.display="none";
    ContainerPolzovatel.style.display="none";
    ContainerZakaz.style.display="none";
    ContainerSize.style.display="none";
    ContainerColor.style.display="flex";
    ContainerBlogs.style.display="none";
    ContainerVopros.style.display="none";
    ContainerPromocod.style.display="none";
    ContainerOtzvs.style.display="none";
})
blog.addEventListener('click', () =>{
    ContainerGlavna.style.display="none";
    ContainerTovars.style.display="none";
    ContainerPolzovatel.style.display="none";
    ContainerZakaz.style.display="none";
    ContainerSize.style.display="none";
    ContainerColor.style.display="none";
    ContainerBlogs.style.display="block";
    ContainerVopros.style.display="none";
    ContainerPromocod.style.display="none";
    ContainerOtzvs.style.display="none";
})
vopros.addEventListener('click', () =>{
    ContainerGlavna.style.display="none";
    ContainerTovars.style.display="none";
    ContainerPolzovatel.style.display="none";
    ContainerZakaz.style.display="none";
    ContainerSize.style.display="none";
    ContainerColor.style.display="none";
    ContainerBlogs.style.display="none";
    ContainerVopros.style.display="block";
    ContainerPromocod.style.display="none";
    ContainerOtzvs.style.display="none";
})
promocod.addEventListener('click', () =>{
    ContainerPromocod.style.display="block";
    ContainerGlavna.style.display="none";
    ContainerTovars.style.display="none";
    ContainerPolzovatel.style.display="none";
    ContainerZakaz.style.display="none";
    ContainerSize.style.display="none";
    ContainerColor.style.display="none";
    ContainerBlogs.style.display="none";
    ContainerVopros.style.display="none";
    ContainerOtzvs.style.display="none";
})
otzv.addEventListener('click', () =>{
    ContainerPromocod.style.display="none";
    ContainerGlavna.style.display="none";
    ContainerTovars.style.display="none";
    ContainerPolzovatel.style.display="none";
    ContainerZakaz.style.display="none";
    ContainerSize.style.display="none";
    ContainerColor.style.display="none";
    ContainerBlogs.style.display="none";
    ContainerVopros.style.display="none";
    ContainerOtzvs.style.display="block";
})
document.addEventListener('DOMContentLoaded', function () {
    const editButtons = document.querySelectorAll('.edit-button');
    const redContainer = document.getElementById('redContainer');
    const closeRedButton = document.querySelector('.closered');
    const editForm = document.getElementById('editForm');

    editButtons.forEach(button => {
        button.addEventListener('click', function () {
            const product = JSON.parse(this.getAttribute('data-product'));

            editForm.action = `/admin/product/${product.id}/update`;
            editForm.querySelector('#brand').value = product.brand;
            editForm.querySelector('#model').value = product.model;
            editForm.querySelector('#price').value = product.price;
            editForm.querySelector('#count').value = product.count;
            editForm.querySelector('#description').value = product.description;

            redContainer.style.display = 'block';
        });
    });

    closeRedButton.addEventListener('click', function () {
        redContainer.style.display = 'none';
    });
});
document.getElementById('RedBlog').addEventListener('click',()=>{
    document.getElementById('redblogs').style.display="block";
})

document.getElementById('addUser').addEventListener('click',()=>{
    document.getElementById('addUserModal').style.display="block";
})

document.querySelector('.closeAdduser').addEventListener('click',()=>{
    document.getElementById('addUserModal').style.display="none";
})
// document.querySelector('.closered').addEventListener('click',()=>{
//     document.getElementById('redContainer').style.display="none";
// })
document.querySelector('.closeOtvet').addEventListener('click',()=>{
    document.getElementById('blockotv').style.display="none";
})
document.querySelector('.closeredblog').addEventListener('click',()=>{
    document.getElementById('redblogs').style.display="none";
})
document.addEventListener('DOMContentLoaded', () => {
    const vborTovarsElements = document.querySelectorAll('.vbor-tovars');

    vborTovarsElements.forEach((element) => {
        element.addEventListener('click', () => {
            element.classList.toggle('active');
        });
    });
});

document.querySelector('.delete-user').addEventListener('click',()=>{
    alert("Вы удалили пользователя под именем:Vlad")
})

// -----------------------------------------------------
// document.addEventListener('DOMContentLoaded', function() {
//     const form = document.getElementById('add-post-form');
//     const blogPostsSection = document.querySelector('.blog-posts');
//
//     form.addEventListener('submit', function(event) {
//         event.preventDefault();
//
//         const title = document.getElementById('title').value;
//         const date = document.getElementById('date').value;
//         const description = document.getElementById('description').value;
//
//         const post = document.createElement('article');
//         post.classList.add('post');
//
//         const postTitle = document.createElement('h2');
//         postTitle.textContent = title;
//
//         const postDate = document.createElement('p');
//         postDate.classList.add('date');
//         postDate.textContent = new Date(date).toLocaleDateString('ru-RU');
//
//         const postDescription = document.createElement('p');
//         postDescription.classList.add('description');
//         postDescription.textContent = description;
//
//         const readMoreButton = document.createElement('button');
//         readMoreButton.classList.add('read-more');
//         readMoreButton.textContent = 'Читать далее';
//         readMoreButton.addEventListener('click', function() {
//             alert('Читать далее');
//         });
//
//         post.appendChild(postTitle);
//         post.appendChild(postDate);
//         post.appendChild(postDescription);
//         post.appendChild(readMoreButton);
//
//         blogPostsSection.appendChild(post);
//
//         form.reset();
//     });
// });
//


function toggleActive(checkbox) {
    var vborTovars = checkbox.closest('.vbor-tovars');
    if (checkbox.checked) {
        vborTovars.classList.add('active');
    } else {
        vborTovars.classList.remove('active');
    }
}
