@if(in_array(request()->route()->getName(), ['busket.index', 'user.index', 'information']))
    <footer>
        <div class="coontainer">
            <div class="blocks">
                <div class="block-1">
                    <div class="top-text">Каталог</div>
                    <div class="mid-txt">
                        <a href="">Обувь</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Бренды</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Модели</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Информация</a>
                    </div>
                </div>
                <div class="block-2">
                    <div class="top-text">Информация</div>
                    <div class="mid-txt">
                        <a href="">Блог</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Контакты</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Доставка</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Оплата</a>
                    </div>
                </div>
                <div class="block-3">
                    <div class="top-text">Контакты</div>
                    <div class="mid-txt">
                        <a href="">vladstrok23@gmail.com</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">+375445656947</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Месенджеры</a>
                        <div class="mes">
                            <a href="https://web.telegram.org/k/"><img src="{{asset('assets/image/snekers/tg.svg')}}" alt=""></a>
                            <a href=""><img src="{{asset('assets/image/snekers/tlf.svg')}}" alt=""></a>
                        </div>
                    </div>
                    <div class="mid-txt">
                        <a href="">Соц.сети</a>
                        <div class="mes">
                            <a href="https://vk.com/feed"><img src="{{asset('assets/image/snekers/vk.svg')}}" alt=""></a>
                            <a href="https://www.instagram.com/nnastten?igsh=MXhtM3U5YmI5bTVzZQ%3D%3D&utm_source=qr"><img src="{{asset('assets/image/snekers/inst.svg')}}" alt=""></a>
                        </div>

                    </div>
                </div>
                <div class="block-4"></div>
            </div>
            <div class="bot-block">
                <div class="prava">© Sneaker-Haven.bu, 2023 — 2025. Интернет - магазин продажи кросовок. Все права защищены.</div>
            </div>

        </div>
    </footer>
@else
    <footer>
        <div class="coontainer">
            <div class="block">
                <div class="block-1">
                    <div class="top-text">Каталог</div>
                    <div class="mid-txt">
                        <a href="">Обувь</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Бренды</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Расчет стоимости</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Информация</a>
                    </div>
                </div>
                <div class="block-2">
                    <div class="top-text">Информация</div>
                    <div class="mid-txt">
                        <a href="">Блог</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Контакты</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Доставка</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Оплата</a>
                    </div>
                </div>
                <div class="block-3">
                    <div class="top-text">Контакты</div>
                    <div class="mid-txt">
                        <a href="">vladstrok23@gmail.com</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">+375445656947</a>
                    </div>
                    <div class="mid-txt">
                        <a href="">Месенджеры</a>
                        <div class="mes">
                            <a href="https://web.telegram.org/k/"><img src="{{asset('assets/image/snekers/tg.svg')}}" alt=""></a>
                            <a href=""><img src="{{asset('assets/image/snekers/tlf.svg')}}" alt=""></a>
                        </div>
                    </div>
                    <div class="mid-txt">
                        <a href="">Соц.сети</a>
                        <div class="mes">
                            <a href="https://vk.com/feed"><img src="{{asset('assets/image/snekers/vk.svg')}}" alt=""></a>
                            <a href="https://www.instagram.com/nnastten?igsh=MXhtM3U5YmI5bTVzZQ%3D%3D&utm_source=qr"><img src="{{asset('assets/image/snekers/inst.svg')}}" alt=""></a>
                        </div>

                    </div>
                </div>
                <div class="block-4"></div>
            </div>
            <div class="bot-block">
                <div class="prava">© Sneaker-Haven.bu, 2023 — 2025. Интернет - магазин продажи кросовок. Все права защищены.</div>
            </div>

        </div>
    </footer>
@endif
