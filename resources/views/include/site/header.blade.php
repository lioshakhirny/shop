<header>
    <div class="header">
        <div class="logo">
            <a href="/"><img src="{{asset('assets/image/Group 362.svg')}}" alt=""></a></div>
        <nav class="one">
            <ul class="topmenu">
                <li>
                    <a href="/">Главная</a>
                </li>
                <li>
                    <a href="{{route('catalog.index')}}">Каталог</a>
                </li>
                <li><a href="{{route('blog')}}">Блог</a></li>
                <li><a href="{{route('contact.index')}}">Контакты</a></li>
                <li><a href="{{route('information')}}">Информация</a></li>
            </ul>
        </nav>
        <div class="burger-icon">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="white" fill-rule="evenodd" d="M3.5 5a1 1 0 0 0 0 2h17a1 1 0 1 0 0-2zm-1 7a1 1 0 0 1 1-1h17a1 1 0 1 1 0 2h-17a1 1 0 0 1-1-1m0 6.001a1 1 0 0 1 1-1h17a1 1 0 1 1 0 2h-17a1 1 0 0 1-1-1" clip-rule="evenodd" />
            </svg>
        </div>
        <div class="acaunts">
            @auth
                @if(auth()->user()->role_id == \App\Enum\Role\RoleEnum::USER->value)
                    <div class="brg"><img src="{{asset('assets/image/Vector (Stroke).sv')}}" alt=""></div>
                    <div class="name-users">{{auth()->user()->name}}</div>
                    <div class="ak">
                        <a href="{{route('user.index')}}"><img class="lk" src="{{asset('assets/image/header/ak.svg')}}"
                                                               alt=""></a>
                    </div>
                    <div class="pocup">
                        <a href="{{route('busket.index')}}">
                            <div class="sumka"><img class="lk" src="{{asset('assets/image/header/sumka.svg')}}" alt="">
                            </div>
                        </a>
                    </div>
                @else
                    <a href="{{route('admin.index')}}">Админ панель</a>
                @endif
            @else
                <a href="{{route('login.index')}}">Войти/Регистрация</a>
            @endauth
        </div>


    </div>
    <div class="burger-menu">
        <div class="closse-burg">
            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                <path fill="white" d="M6.4 19L5 17.6l5.6-5.6L5 6.4L6.4 5l5.6 5.6L17.6 5L19 6.4L13.4 12l5.6 5.6l-1.4 1.4l-5.6-5.6z" />
            </svg>
        </div>
        <div class="burger-menu-items">
            <ul class="burg-mens">
                <li>
                    <a href="/">Главная</a>
                </li>
                <li>
                    <a href="{{route('catalog.index')}}">Каталог</a>
                </li>
                <li><a href="{{route('blog')}}">Блог</a></li>
                <li><a href="{{route('contact.index')}}">Контакты</a></li>
                <li><a href="{{route('information')}}">Информация</a></li>
            </ul>
            <div class="acauntss">
                @auth
                    @if(auth()->user()->role_id == \App\Enum\Role\RoleEnum::USER->value)
                        <div class="brg"><img src="{{asset('assets/image/Vector (Stroke).sv')}}" alt=""></div>
                        <div class="name-users">{{auth()->user()->name}}</div>
                        <div class="ak">
                            <a href="{{route('user.index')}}"><img class="lk" src="{{asset('assets/image/header/ak.svg')}}"
                                                                   alt=""></a>
                        </div>
                        <div class="pocup">
                            <a href="{{route('busket.index')}}">
                                <div class="sumka"><img class="lk" src="{{asset('assets/image/header/sumka.svg')}}" alt="">
                                </div>
                            </a>
                        </div>
                    @else
                        <a href="{{route('admin.index')}}">Админ панель</a>
                    @endif
                @else
                    <a href="{{route('login.index')}}">Войти/Регистрация</a>
                @endauth
            </div>
        </div>
    </div>
</header>
<script>
    document.querySelector('.burger-icon').addEventListener('click', function() {
        document.querySelector('.burger-menu').style.display = 'block';
    })
    document.querySelector('.closse-burg').addEventListener('click', function() {
        document.querySelector('.burger-menu').style.display = 'none';
    })
</script>
