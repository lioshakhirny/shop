<nav>
    <div class="logo-name">
        <div class="logo-image">
            <img src="" alt="">
        </div>
    </div>
    <div class="menu-items">
        <ul class="nav-links">
            <li class="gl">
                <a href="#">
                    <i class="uil uil-estate"></i>
                    <span class="link-name">Главная</span>
                </a>
            </li>
            <li class="tv"><a href="#">
                    <i>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M6 11.5V6h5.5v5.5zM6 18v-5.5h5.5V18zm6.5-6.5V6H18v5.5zm0 6.5v-5.5H18V18zM5 21q-.825 0-1.412-.587T3 19V5q0-.825.588-1.412T5 3h14q.825 0 1.413.588T21 5v14q0 .825-.587 1.413T19 21zm0-2h14V5H5z" />
                        </svg>
                    </i>
                    <span class="link-name ">Товары</span>
                </a></li>
            <li class="bl">
                <a href="#">
                    <i class="uil uil-chart"></i>
                    <span class="link-name">Блог</span>
                </a>
            </li>
            <li class="pl">
                <a href="#">
                    <i>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M17 21.025q-.2 0-.4-.05t-.375-.175l-3-1.75q-.35-.2-.537-.537t-.188-.738V14.25q0-.4.188-.737t.537-.538l3-1.75q.175-.125.375-.175T17 11t.388.063t.362.162l3 1.75q.35.2.55.538t.2.737v3.525q0 .4-.2.738t-.55.537l-3 1.75q-.175.1-.363.163t-.387.062M10 12q-1.65 0-2.825-1.175T6 8t1.175-2.825T10 4t2.825 1.175T14 8t-1.175 2.825T10 12m-8 8v-2.8q0-.825.425-1.55t1.175-1.1q1.275-.65 2.875-1.1T10 13h.35q.15 0 .3.05q-.2.45-.337.938T10.1 15H10q-1.775 0-3.187.45t-2.313.9q-.225.125-.363.35T4 17.2v.8h6.3q.15.525.4 1.038t.55.962zm8-10q.825 0 1.413-.587T12 8t-.587-1.412T10 6t-1.412.588T8 8t.588 1.413T10 10m4.65 3.85L17 15.225l2.35-1.375L17 12.5zm3.1 5.2l2.25-1.3V15l-2.25 1.325zM14 17.75l2.25 1.325V16.35L14 15.025z" />
                        </svg>
                    </i>
                    <span class="link-name ">Пользователи</span>
                </a></li>
            <li class="vp">
                <a href="#">
                    <i class="uil uil-comments"></i>
                    <span class="link-name">Вопросы</span>
                </a>
            </li>
            <li class="zk">
                <a href="#">
                    <i>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M10.6 16.05L17.65 9l-1.4-1.4l-5.65 5.65l-2.85-2.85l-1.4 1.4zM5 21q-.825 0-1.412-.587T3 19V5q0-.825.588-1.412T5 3h4.2q.325-.9 1.088-1.45T12 1t1.713.55T14.8 3H19q.825 0 1.413.588T21 5v14q0 .825-.587 1.413T19 21zm0-2h14V5H5zm7-14.75q.325 0 .538-.213t.212-.537t-.213-.537T12 2.75t-.537.213t-.213.537t.213.538t.537.212M5 19V5z" />
                        </svg>
                    </i>
                    <span class="link-name ">Заказы</span>
                </a>
            </li>
            <li class="promocod">
                <a href="#">
                    <i>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 512 512">
                            <path fill="black" d="M213.3 0h-128C38.2 0 0 38.2 0 85.3v128L298.7 512L512 298.7zm-128 128c-23.6 0-42.7-19.1-42.7-42.7s19.1-42.7 42.7-42.7S128 61.8 128 85.3S108.9 128 85.3 128m85.4 192L320 170.7l42.7 42.7l-149.4 149.3zm85.3 85.3L405.3 256l42.7 42.7L298.7 448z" />
                        </svg>
                    </i>
                    <span class="link-name ">Промокоды</span>
                </a>
            </li>
            <li class="color-menu"><a href="#">
                    <i>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M8.65 20.5L2.5 14.35q-.25-.25-.375-.55T2 13.175t.125-.625T2.5 12l5.75-5.725L6.375 4.4q-.325-.325-.337-.775t.312-.8t.8-.35t.825.35L17.15 12q.25.25.363.55t.112.625t-.112.625t-.363.55L11 20.5q-.25.25-.55.375T9.825 21t-.625-.125t-.55-.375M9.825 7.85l-5.35 5.35h10.7zM19.8 21q-.9 0-1.525-.638T17.65 18.8q0-.675.338-1.275t.762-1.175l.475-.6q.225-.275.588-.287t.587.262l.5.625q.4.575.75 1.175T22 18.8q0 .925-.65 1.563T19.8 21" />
                        </svg>
                    </i>
                    <span class="link-name ">Цвета на обувь</span>
                </a>
            </li>
            <li class="otz">
                <a href="#">
                    <i>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M14.5 15q.825 0 1.413-.587T16.5 13t-.587-1.412T14.5 11q-.375 0-.712.138t-.613.362L10.5 10.15v-.3l2.675-1.35q.275.225.613.363T14.5 9q.825 0 1.413-.587T16.5 7t-.587-1.412T14.5 5t-1.412.588T12.5 7v.15L9.825 8.5q-.275-.225-.612-.363T8.5 8q-.825 0-1.412.588T6.5 10t.588 1.413T8.5 12q.375 0 .713-.137t.612-.363l2.675 1.35V13q0 .825.588 1.413T14.5 15M2 22V4q0-.825.588-1.412T4 2h16q.825 0 1.413.588T22 4v12q0 .825-.587 1.413T20 18H6zm3.15-6H20V4H4v13.125zM4 16V4z" />
                        </svg>
                    </i>
                    <span class="link-name ">Отзывы</span>
                </a>
            </li>
            <li class="size-snek"><a href="#">
                    <i>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="currentColor" d="M4 18q-.825 0-1.412-.587T2 16V8q0-.825.588-1.412T4 6h16q.825 0 1.413.588T22 8v8q0 .825-.587 1.413T20 18zm0-2h16V8h-3v3q0 .425-.288.713T16 12t-.712-.288T15 11V8h-2v3q0 .425-.288.713T12 12t-.712-.288T11 11V8H9v3q0 .425-.288.713T8 12t-.712-.288T7 11V8H4zm8-4" />
                        </svg>
                    </i>
                    <span class="link-name ">Размеры на обувь</span>
                </a>
            </li>
            <li class="zk">
                <a href="/">
                    <i class="uil uil-share"></i>
                    <span class="link-name">Вернуться на сайт</span>
                </a>
            </li>
        </ul>
        <ul class="logout-mode">
            <li><a href="{{route('admin.logout')}}">
                    <i class="uil uil-signout"></i>
                    <span class="link-name">Выход</span>
                </a></li>

            <li class="mode">
                <a href="#">
                    <i class="uil uil-moon"></i>
                    <span class="link-name">Dark Mode</span>
                </a>

                <div class="mode-toggle">
                    <span class="switch"></span>
                </div>
            </li>
        </ul>
    </div>
</nav>
