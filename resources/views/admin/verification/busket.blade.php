<style>
    table, th, td {
        border: 1px solid;
    }

    table {
        width: 100%;
    }
</style>
<table>
    <caption>Чек</caption>
    <thead>
    <tr>
        <th>Бренд</th>
        <th>Модель</th>
        <th>Описание</th>
        <th>Количество</th>
        <th>Цена</th>
    </tr>
    </thead>
    <tbody>
    @foreach($carts as $cart)
        <tr>
            <th>{{ $cart['brand'] }}</th>
            <td>{{ $cart['model'] }}</td>
            <td>{{ $cart['description'] }}</td>
            <td>{{ $cart['count'] }}</td>
            <td>{{ $cart['price'] }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<p id="custom-total-amount">Общая сумма: <span>{{array_sum(array_column($carts, 'price'))}}</span> руб.</p>
