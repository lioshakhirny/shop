@extends('layouts.admin.admin')
@section('content')
    <section class="dashboard">
        <div class="top">
            <i class="uil uil-bars sidebar-toggle"></i>
            <div class="search-box">
                <i class="uil uil-search"></i>
                <input type="text" placeholder="Search here...">
            </div>
        </div>
        <div class="dash-content">
            <div class="overview">
                <div class="title">
                    <i class="uil uil-tachometer-fast-alt"></i>
                    <span class="text">Панель администратора</span>
                </div>

                <div class="boxes">
                    <div class="box box1">
                        <i>
                            <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24">
                                <path fill="currentColor" d="M6 11.5V6h5.5v5.5zM6 18v-5.5h5.5V18zm6.5-6.5V6H18v5.5zm0 6.5v-5.5H18V18zM5 21q-.825 0-1.412-.587T3 19V5q0-.825.588-1.412T5 3h14q.825 0 1.413.588T21 5v14q0 .825-.587 1.413T19 21zm0-2h14V5H5z" />
                            </svg>
                        </i>
                        <span class="text">Всего товаров</span>
                        <span class="number">{{$product_count}}</span>
                    </div>
                    <div class="box box2">
                        <i>
                            <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24">
                                <path fill="currentColor" d="m12 10l-1.4-1.4L12.175 7H8V5h4.175l-1.6-1.6L12 2l4 4zM7 22q-.825 0-1.412-.587T5 20t.588-1.412T7 18t1.413.588T9 20t-.587 1.413T7 22m10 0q-.825 0-1.412-.587T15 20t.588-1.412T17 18t1.413.588T19 20t-.587 1.413T17 22M1 4V2h3.275l4.25 9h7l3.9-7H21.7l-4.4 7.95q-.275.5-.737.775T15.55 13H8.1L7 15h12v2H7q-1.125 0-1.713-.975T5.25 14.05L6.6 11.6L3 4z" />
                            </svg>
                        </i>
                        <span class="text">Покупок</span>
                        <span class="number">0</span>
                    </div>
                    <div class="box box3">
                        <i>
                            <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24">
                                <path fill="currentColor" d="M17 21.025q-.2 0-.4-.05t-.375-.175l-3-1.75q-.35-.2-.537-.537t-.188-.738V14.25q0-.4.188-.737t.537-.538l3-1.75q.175-.125.375-.175T17 11t.388.063t.362.162l3 1.75q.35.2.55.538t.2.737v3.525q0 .4-.2.738t-.55.537l-3 1.75q-.175.1-.363.163t-.387.062M10 12q-1.65 0-2.825-1.175T6 8t1.175-2.825T10 4t2.825 1.175T14 8t-1.175 2.825T10 12m-8 8v-2.8q0-.825.425-1.55t1.175-1.1q1.275-.65 2.875-1.1T10 13h.35q.15 0 .3.05q-.2.45-.337.938T10.1 15H10q-1.775 0-3.187.45t-2.313.9q-.225.125-.363.35T4 17.2v.8h6.3q.15.525.4 1.038t.55.962zm8-10q.825 0 1.413-.587T12 8t-.587-1.412T10 6t-1.412.588T8 8t.588 1.413T10 10m4.65 3.85L17 15.225l2.35-1.375L17 12.5zm3.1 5.2l2.25-1.3V15l-2.25 1.325zM14 17.75l2.25 1.325V16.35L14 15.025z" />
                            </svg>
                        </i>
                        <span class="text">Кол-во пользователей</span>
                        <span class="number">{{$users->count()}}</span>
                    </div>
                </div>
            </div>
            <div class="activity">
                <div class="title">
                    <i class="uil uil-clock-three"></i>
                    <span class="text">Новый зарегистрированный пользователь</span>
                </div>

                <div class="activity-data">
                    <div class="data names">
                        <span class="data-title">Имя</span>
                        <span class="data-list">{{$newUser->name ?? null}}</span>
                    </div>
                    <div class="data email">
                        <span class="data-title">Почта</span>
                        <span class="data-list">{{$newUser->email ?? null}}</span>
                    </div>
                    <div class="data joined">
                        <span class="data-title">Зарегистрировался</span>
                        <span class="data-list">{{$newUser->created_at ?? null}}</span>
                    </div>
                    <div class="data type">
                        <span class="data-title">Тип</span>
                        <span class="data-list">Новый пользователь</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="tovars">
            <div class="button-tovars">
                <button id="addTovar" class="add">Добавить</button>
            </div>
            <h2>Товары</h2>
            @foreach($products as $product)
                <div class="activity-data">
                    <div class="data names">
                        <span class="data-title">ID:</span>
                        <span class="data-list">{{$product->id}}</span>
                    </div>
                    <div class="data kategoria">
                        <span class="data-title">Бренд</span>
                        <span class="data-list">{{$product->brand}}</span>
                    </div>
                    <div class="data brends">
                        <span class="data-title">Модель</span>
                        <span class="data-list">{{$product->model}}</span>
                    </div>
                    <div class="data sizez">
                        <span class="data-title">Цена:</span>
                        <span class="data-list">{{$product->price}}</span>
                    </div>
                    <div class="data kol-sklad">
                        <span class="data-title">Кол-во на складе:</span>
                        <span class="data-list">{{$product->count}}</span>
                    </div>
                    <div class="data kol-sklad">
                        <span class="data-title">Действия:</span>
                        <form method="post" action="{{route('admin.product.delete', ['id' => $product->id])}}">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="data-list-l">Удалить</button>
                        </form>
                        <span class="data-list-l edit-button" data-product='@json($product)'>Редактировать</span>
                    </div>
                </div>
            @endforeach

            @if(!empty($product))
                <div id="redContainer" class="container-red" style="display:none;">
                    <div class="obert">
                        <span>Редактировать товар</span>
                        <span class="closered">&times;</span>
                    </div>
                    <form class="container-input" action="{{ route('admin.product.update', ['id' => $product->id]) }}" enctype="multipart/form-data" method="post" id="editForm">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="brand">Бренд:</label>
                            <input type="text" id="brand" name="brand" required>
                        </div>
                        <div class="form-group">
                            <label for="model">Модель:</label>
                            <input type="text" id="model" name="model" required>
                        </div>
                        <div class="form-group">
                            <label for="price">Цена:</label>
                            <input type="number" id="price" name="price" required>
                        </div>
                        <div class="form-group">
                            <label for="count">Количество в наличии:</label>
                            <input type="number" id="count" name="count" required>
                        </div>
                        <div class="form-group">
                            <label for="description">Описание товара</label>
                            <br>
                            <textarea rows="20" cols="53" name="description" id="description"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="image">Изображение товара:</label>
                            <input type="file" id="image" name="images[]" accept="image/*" multiple>
                        </div>
                        <button type="submit">Изменить</button>
                    </form>
                </div>
            @endif

            <div id="addContainer" class="container-add">
                <div class="obert">
                    <span>Добавить товар</span>
                    <span class="close">&times;</span>
                </div>
                <form class="container-input" action="{{route('admin.product.store')}}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="brand">Бренд:</label>
                        <input type="text" id="brand" name="brand" required>
                    </div>
                    <div class="form-group">
                        <label for="brand">Модель:</label>
                        <input type="text" id="brand" name="model" required>
                    </div>
                    <div class="form-group">
                        <label for="price">Цена:</label>
                        <input type="number" id="price" name="price" required>
                    </div>
                    <div class="form-group">
                        <label for="quantity">Количество в наличии:</label>
                        <input type="number" id="quantity" name="count" required>
                    </div>
                    <div class="form-group">
                        <label for="description">Описание товара</label>
                        <br>
                        <textarea rows="20" cols="53" name="description"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="image">Изображение товара:</label>
                        <input type="file"  id="image" name="images[]" accept="image/*" multiple>
                        <input type="file"  id="image" name="images[]" accept="image/*" multiple>
                        <input type="file"  id="image" name="images[]" accept="image/*" multiple>
                    </div>
                    <button type="submit">Добавить</button>
                </form>
            </div>
        </div>
        <div class="usser">
            <div class="button-tovars">
                <button id="addUser" class="add">Добавить</button>
            </div>
            <div id="addUserModal" class="modal">
                <div class="modal-content">
                    <span class="closeAdduser">&times;</span>
                    <h2>Добавить пользователя</h2>
                    <form id="addUserForm">
                        <label for="userName">Имя</label>
                        <input type="text" id="userName" required>

                        <label for="userEmail">Почта</label>
                        <input type="email" id="userEmail" required>

                        <label for="userAddress">Адрес</label>
                        <input type="text" id="userAddress" required>

                        <label for="userDate">Дата создания</label>
                        <input type="date" id="userDate" required>

                        <button type="submit" class="btn">Добавить</button>
                    </form>
                </div>
            </div>
            <h2>Пользователи</h2>
            @foreach($users as $user)
            <div class="activity-data">
                <div class="data names">
                    <span class="data-title">Id:</span>
                    <span class="data-list">{{$user->id}}</span>
                </div>
                <div class="data kategoria">
                    <span class="data-title">Имя</span>
                    <span class="data-list">{{$user->name}}</span>
                </div>
                <div class="data brends">
                    <span class="data-title">Почта</span>
                    <span class="data-list">{{$user->email}}</span>
                </div>
                <div class="data sizez">
                    <span class="data-title">Адресс:</span>
                    <span class="data-list">г. Гроно</span>
                </div>
                <div class="data kol-sklad">
                    <span class="data-title">Дата создание:</span>
                    <span class="data-list">{{$user->created_at}}</span>
                </div>
                <form method="post" action="{{ route('admin.user.delete', ['id' => $user->id]) }}">
                    @method('DELETE')
                    @csrf
                    <button type="submit" class="delete-user">
                        <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" viewBox="0 0 24 24">
                            <path fill="currentColor"
                                  d="M7 21q-.825 0-1.412-.587T5 19V6H4V4h5V3h6v1h5v2h-1v13q0 .825-.587 1.413T17 21zM17 6H7v13h10zM9 17h2V8H9zm4 0h2V8h-2zM7 6v13z"/>
                        </svg>
                    </button>
                </form>
            </div>
            @endforeach
        </div>
        <div class="zakazz">
            <h2>Заказы</h2>
            <div class="container-zakazs">
                @foreach($orders as $order)
                    <div class="container-zakaz">
                        <div class="txt-zakaz">
                            <label>IdЗаказа:</label>
                            <span>{{ $order->id }}</span>
                        </div>
                        <div class="txt-zakaz">
                            <label>Пользователь:</label>
                            <span>{{ \App\Models\User::query()->find($order->user_id)->name }}</span>
                        </div>
                        <div class="txt-zakaz">
                            <label>Id товара:</label>
                            <span>{{ $order->product_id }}</span>
                        </div>
                        <div class="txt-zakaz">
                            <label>Модель:</label>
                            <span>{{ \App\Models\Product::query()->find($order->product_id)->model }}</span>
                        </div>
                        <div class="txt-zakaz">
                            <label>Количество:</label>
                            <span>{{ $order->count }}</span>
                        </div>
                        <div class="txt-zakaz">
                            <label>Сумма:</label>
                            <span>{{ $order->sum }}</span>
                        </div>
                        <div class="txt-zakaz">
                            <label>Дата заказа:</label>
                            <span>{{$order->time}}</span>
                        </div>
                        <div class="txt-zakaz">
                            <label>Адрес:</label>
                            @php
                                $address = \App\Models\Adress::query()->find($order->adress_id);
                            @endphp
                            @if ($address)
                                <span>{{ $address->city }}, {{ $address->country }}, {{ $address->house}}</span>
                            @else
                                <span>Адрес не найден</span>
                            @endif
                        </div>

                        <form action="{{ route('admin.order.update', ['id' => $order->id]) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="txt-zakaz">
                                <label>Статус заказа:</label>
                                <select name="status" id="custom-payment-method" name="custom-payment-method">
                                    @foreach(\App\Enum\Order\StatusEnum::cases() as $enum)
                                        <option @selected($enum->value === $order->status) value="{{$enum->value}}">{{ $enum->lable() }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button class="delete-zakaz" type="submit">Обновить</button>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="size-sneakers">
            <form method="post" action="{{route('admin.shoe-size.store')}}">
                @csrf
                <div class="shoe-sizes">
                    <h2>Выберите размер обуви</h2>
                    <div class="sizes">
                        <label class="size">
                            <input type="checkbox" name="size[]" value="33">
                            <span>33</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="34">
                            <span>34</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="35">
                            <span>35</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="36">
                            <span>36</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="37">
                            <span>37</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="38">
                            <span>38</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="39">
                            <span>39</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="40">
                            <span>40</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="41">
                            <span>41</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="42">
                            <span>42</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="43">
                            <span>43</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="44">
                            <span>44</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="45">
                            <span>45</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="46">
                            <span>46</span>
                        </label>
                        <label class="size">
                            <input type="checkbox" name="size[]" value="47">
                            <span>47</span>
                        </label>
                    </div>
                </div>
                <div class="tovars-size-block">
                    <h2>Выберите обувь</h2>
                    @foreach($products as $product)
                        <div class="vbor-tovars">
                            <div class="datas kateg ">
                                <label>
                                    <input type="checkbox" name="product_id" onchange="toggleActive(this)" value="{{$product->id}}" >
                                    <span class="data-list">{{$product->brand}}</span>
                                    <span class="data-list">{{$product->model}}</span>
                                </label>

                            </div>

                        </div>
                    @endforeach
                </div>
                <div class="add-sizez">
                    <button type="submit">Добавить размер на обувь</button>
                </div>
            </form>
        </div>
        <div class="color-sneaker">
            <form method="post" action="{{route('admin.color.store')}}">
                @csrf
                <div class="shoe-colors">
                    <h2>Выберите цвет обуви</h2>
                    <div class="colors">
                        <label class="color">
                            <input type="radio" name="color[]" value="red">
                            <span class="color-box" style="background-color: red;"></span>
                        </label>
                        <label class="color">
                            <input type="radio" name="color[]" value="blue">
                            <span class="color-box" style="background-color: #0033ff;"></span>
                        </label>
                        <label class="color">
                            <input type="radio" name="color[]" value="violet">
                            <span class="color-box" style="background-color: #dd00ff;"></span>
                        </label>
                        <label class="color">
                            <input type="radio" name="color[]" value="yellow">
                            <span class="color-box" style="background-color: yellow;"></span>
                        </label>
                        <label class="color">
                            <input type="radio" name="color[]" value="orange">
                            <span class="color-box" style="background-color: orange;"></span>
                        </label>
                        <label class="color">
                            <input type="radio" name="color[]" value="pink">
                            <span class="color-box" style="background-color: #d00179;"></span>
                        </label>
                        <label class="color">
                            <input type="radio" name="color[]" value="white">
                            <span class="color-box" style="background-color: white; border: 1px solid #ccc;"></span>
                        </label>
                        <label class="color">
                            <input type="radio" name="color[]" value="black">
                            <span class="color-box" style="background-color: black;"></span>
                        </label>
                    </div>
                </div>
                <div class="tovars-size-block">
                    <h2>Выберите обувь</h2>
                    @foreach($products as $product)
                        <div class="vbor-tovars">
                            <div class="datas kateg ">
                                <input type="checkbox" value="{{$product->id}}" name="product_id">
                                <span class="data-list">{{$product->brand}}</span>
                            </div>
                            <div class="datas brends">
                                <span class="data-list">{{$product->model}}</span>
                            </div>
                        </div>
                    @endforeach
                </div>
                <div class="add-sizez">
                    <button>Добавить цвет на обувь</button>
                </div>
            </form>
        </div>
        <div class="containerr-blogs">
            <section class="panel-blogs">
                <h2>Добавить новый пост</h2>
                <form id="" action="{{ route('admin.blog.store') }}" method="post">
                    @csrf
                    <div>
                        <label for="title">Название:</label>
                        <input type="text" id="title" name="title" required>
                    </div>
                    <div>
                        <label for="date">Дата:</label>
                        <input type="date" id="date" name="date" required>
                    </div>
                    <div>
                        <label for="description">Описание:</label>
                        <textarea id="description" name="description" rows="4" required></textarea>
                    </div>
                    <button type="submit">Добавить пост</button>
                </form>
            </section>
            <section class="blog-posts">
                <section class="blog-posts">
                    @foreach($blogs as $blog)
                        <article class="post">
                            <h2>{{ $blog->title }}</h2>
                            <p class="date">{{ $blog->created_at }}</p>
                            <p class="description">{{ $blog->description }}</p>
                            <div class="block-btn">
                                <form action="{{ route('admin.blog.delete', ['id' => $blog->id]) }}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="delete-blog">Удалить</button>
                                </form>
                                <button type="submit" id="RedBlog" class="delete-blog">Редактировать</button>
                            </div>
                        </article>
                    @endforeach
                </section>
                <div id="redblogs" class="container-red-blog">
                    <div class="obert">
                        <span>Редактировать блога</span>
                        <span class="closeredblog">&times;</span>
                    </div>
                    @foreach($blogs as $blog)
                        <form class="container-input" action="{{ route('admin.blog.update', ['id' => $blog->id]) }}" method="post">
                            @method('PATCH')
                            @csrf
                            <div class="lll">
                                <label for="title">Название:</label>
                                <input type="text" id="title" name="title" value="{{ $blog->title }}" required>
                            </div>
                            <div class="lll">
                                <label for="date">Дата:</label>
                                <input type="date" id="date" name="date" value="{{ $blog->created_at }}" required>
                            </div>
                            <div class="lll">
                                <label for="description">Описание:</label>
                                <textarea id="description" name="description" rows="4" required>{{ $blog->description }}</textarea>
                            </div>
                            <button type="submit">Изменить</button>
                        </form>
                    @endforeach
                </div>
            </section>
        </div>
        <div class="container-vopros">
            <h2>Форма для вопросов</h2>
            @foreach($contacts as $contact)
            <form class="otVopr" method="post" action="{{route('admin.answer.store', ['id' => $contact->id])}}" id="blockotv">
                @csrf
                <div class="blockHeaddde">
                    <h2>Добавить пользователя</h2>
                    <span class="closeOtvet">&times;</span>
                </div>
                <div class="form-group">
                    <label for="userEmail">Имя пользователя:</label>
                    <span>{{ $contact->name }}</span>
                </div>
                <div class="form-group">
                    <label for="userQuestion">Вопрос:</label>
                    <p>{{ $contact->description }}</p>
                </div>
                <div class="form-group">
                    <label for="adminResponse">Ответ администратора:</label>
                    <div>
                        <textarea class="form-control" id="adminResponse" name="answer" rows="3" required></textarea>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" >Отправить</button>
            </form>
            @endforeach
            <div class="container-vopr">
                @foreach($contacts as $contact)
                    <div class="block-voprosov">
                        <div class="form-groupp">
                            <label for="userEmail">Имя пользователя:</label>
                            <span>{{ $contact->name }}</span>
                        </div>
                        <div class="form-groupp">
                            <label for="userEmail">Емейл пользователя:</label>
                            <span>{{ $contact->email }}</span>
                        </div>
                        <div class="form-groupp">
                            <label for="userEmail">Телефон пользователя:</label>
                            <span>{{ $contact->phone }}</span>
                        </div>
                        <div class="form-groupp">
                            <label for="userQuestion">Вопрос:</label>
                            <p>{{ $contact->description }}</p>
                        </div>
                        <button type="button" id="otvet" class="btn-vopr">Ответить</button>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="container-promokod">
            <h2>Панель промокодов</h2>
            <div class="block-promocod">
                <form action="{{ route('admin.promocode.store') }}" method="post">
                    @csrf
                    <input type="text" name="code" placeholder="Промокод">
                    <input type="number" name="discount" placeholder="Скидка">
                    <button type="submit" class="btn btn-primary prom">Добавить</button>
                </form>
            </div>
            <div class="otb-promocod">
                <h2>Отображение всех существующих промокодов</h2>
                @foreach($promocodes as $promocode)
                    <div class="block-otb-prom">
                        <span> Промокод: {{ $promocode->code }}</span>
                        <span>Скидка: {{ $promocode->discount }}</span>
                        <form action="{{ route('admin.promocode.delete', ['id' => $promocode->id]) }}" method="post">
                            @method('DELETE')
                            @csrf
                            <button type="submit" class="delete-promocod">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                    <path fill="currentColor"
                                          d="M7 21q-.825 0-1.412-.587T5 19V6H4V4h5V3h6v1h5v2h-1v13q0 .825-.587 1.413T17 21zM17 6H7v13h10zM9 17h2V8H9zm4 0h2V8h-2zM7 6v13z"/>
                                </svg>
                            </button>
                        </form>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="contaiiner-otzv">
            <h2>Отзывы от посетителей</h2>
            <div class="block-otzv">
                @foreach($reviews as $review)
                <div class="reviews-inner">
                        <div class="content-otz">
                            <div class="contents">
                                <span class="author">{{ $review->name }}</span>
                                <div class="date">{{ $review->created_at }}</div>
                                <p>{{ $review->description }}</p>
                                <form action="{{ route('admin.review.delete', ['id' => $review->id]) }}" method="post">
                                    @method('DELETE')
                                    @csrf
                                    <button type="submit" class="btnotzv">Удалить</button>
                                </form>
                            </div>
                        </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
    <script>
        document.getElementById('otvet').addEventListener('click',()=>{
            document.getElementById('blockotv').style.display="block";
        })
    </script>

@endsection
