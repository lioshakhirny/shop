<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Sneaker Haven</title>
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}">
    @if(in_array(request()->route()->getName(), ['catalog.index', 'main', 'login.index', 'contact.index', 'information', 'blog']))
        <link rel="stylesheet" href="{{asset('assets/css/header.css')}}">
    @endif
    <link rel="stylesheet" href="{{asset('assets/css/media.css')}}">
    @if(request()->route()->getName() == 'catalog.index')
        <link rel="stylesheet" href="{{asset('assets/css/katalog.css')}}">
    @endif
    @if(request()->route()->getName() == 'login.index')
        <link rel="stylesheet" href="{{asset('assets/css/vhode.css')}}">
    @endif
    @if(request()->route()->getName() == 'user.index')
        <link rel="stylesheet" href="{{asset('assets/css/kab.css')}}">
    @endif
    @if(request()->route()->getName() == 'busket.index')
        <link rel="stylesheet" href="{{asset('assets/css/karzina.css')}}">
    @endif
    @if(request()->route()->getName() == 'catalog.show')
        <link rel="stylesheet" href="{{asset('assets/css/kartohka.css')}}">
    @endif
    @if(request()->route()->getName() == 'busket.index')
        <link rel="stylesheet" href="{{asset('assets/css/product.css')}}">
    @endif
    @if(request()->route()->getName() == 'contact.index')
        <link rel="stylesheet" href="{{asset('assets/css/svaz.css')}}">
    @endif
    @if(request()->route()->getName() == 'blog')
        <link rel="stylesheet" href="{{asset('assets/css/blogkontakts.css')}}">
    @endif
    @if(request()->route()->getName() == 'information')
        <link rel="stylesheet" href="{{asset('assets/css/opisanie.css')}}">
    @endif
    <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/3.2.1/anime.min.js"
            integrity="sha512-z4OUqw38qNLpn1libAN9BsoDx6nbNFio5lA6CuTp9NlK83b89hgyCVq+N5FdBJptINztxn1Z3SaKSKUS5UP60Q=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://code.jquery.com/jquery-3.7.1.js"
            integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
</head>
<body>
@include('include.site.header')
<div class="wraper">
    <main>
        @yield('content')
    </main>
</div>
@include('include.site.footer')
@if(in_array(request()->route()->getName(), ['catalog.index', 'main', 'login.index']))
    <script src="{{asset('assets/js/script.js')}}"></script>
@endif
@if(request()->route()->getName() == 'user.index')
    <script src="{{asset('assets/js/kab.js')}}"></script>
@endif
@if(request()->route()->getName() == 'busket.index')
    <script src="{{asset('assets/js/korzina.js')}}"></script>
@endif
@if(request()->route()->getName() == 'catalog.show')
    <script src="{{asset('assets/js/kartohka.js')}}"></script>
@endif

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</body>
</html>
