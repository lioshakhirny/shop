@extends('layouts.site.main')
@section('content')
<div class="container">
    <div class="block">
        <div class="galv-txt">Личный кабинет</div>
        <div class="glav-block">
            <div class="left">
                <div class="spisok">
                    <div id="ak" class="ak align">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="black" d="M4 22a8 8 0 1 1 16 0h-2a6 6 0 0 0-12 0zm8-9c-3.315 0-6-2.685-6-6s2.685-6 6-6s6 2.685 6 6s-2.685 6-6 6m0-2c2.21 0 4-1.79 4-4s-1.79-4-4-4s-4 1.79-4 4s1.79 4 4 4" />
                        </svg>
                        <div class="txt-spisok">Мой акаунт</div>
                    </div>
                    <div id="prof" class="prof align">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 32 32">
                            <path fill="black" d="M2 26h28v2H2zM25.4 9c.8-.8.8-2 0-2.8l-3.6-3.6c-.8-.8-2-.8-2.8 0l-15 15V24h6.4zm-5-5L24 7.6l-3 3L17.4 7zM6 22v-3.6l10-10l3.6 3.6l-10 10z" />
                        </svg>
                        <div class="txt-spisok">Редактировать профиль</div>
                    </div>
                    <div class="histori align">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="black" d="M12 2c5.523 0 10 4.477 10 10s-4.477 10-10 10S2 17.523 2 12h2a8 8 0 1 0 1.385-4.5H8v2H2v-6h2V6a9.985 9.985 0 0 1 8-4m1 5v4.585l3.243 3.243l-1.415 1.415L11 12.413V7z" />
                        </svg>
                        <div class="txt-spisok">История заказов</div>
                    </div>
                    <div class="adresa align">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 256 256">
                            <path fill="black" d="M83.19 174.4a8 8 0 0 0 11.21-1.6a52 52 0 0 1 83.2 0a8 8 0 1 0 12.8-9.6a67.9 67.9 0 0 0-27.4-21.69a40 40 0 1 0-53.94 0A67.9 67.9 0 0 0 81.6 163.2a8 8 0 0 0 1.59 11.2M112 112a24 24 0 1 1 24 24a24 24 0 0 1-24-24m96-88H64a16 16 0 0 0-16 16v24H32a8 8 0 0 0 0 16h16v40H32a8 8 0 0 0 0 16h16v40H32a8 8 0 0 0 0 16h16v24a16 16 0 0 0 16 16h144a16 16 0 0 0 16-16V40a16 16 0 0 0-16-16m0 192H64V40h144Z" />
                        </svg>
                        <div class="txt-spisok">Адреса</div>
                    </div>
                    <div class="otz align">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="black" d="M6 14h3.05l5-5q.225-.225.338-.513t.112-.562t-.125-.537t-.325-.488l-.9-.95q-.225-.225-.5-.337t-.575-.113q-.275 0-.562.113T11 5.95l-5 5zm7-6.075L12.075 7zM7.5 12.5v-.95l2.525-2.525l.5.45l.45.5L8.45 12.5zm3.025-3.025l.45.5l-.95-.95zm.65 4.525H18v-2h-4.825zM2 22V4q0-.825.588-1.412T4 2h16q.825 0 1.413.588T22 4v12q0 .825-.587 1.413T20 18H6zm3.15-6H20V4H4v13.125zM4 16V4z" />
                        </svg>
                        <div class="txt-spisok">Отзыв</div>
                    </div>
                    <div class="vihad align"><img src="img/akk/vihod.svg" alt="">
                        <div class="txt-spisok"><a href="{{route('user.logout')}}">Выход</a></div>
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="hello">
                    <div class="hello-txt">Приветствуем {{auth()->user()->name}}</div>
                    <div class="hello-block">
                        <div class="container-right">
                            <div class="line">
                                <div id="dopProf" class="blocki">
                                    <a  href="#">
                                        <img src="img/akk/hello/adres.svg" alt="">
                                        <div class="blocki-txt">Мой профиль</div>
                                    </a>
                                </div>
                                <div id="dopHistori" class="blocki">
                                    <a href="#"><img src="img/akk/hello/Vector.svg" alt="">
                                        <div class="blocki-txt">История заказов</div>
                                    </a>
                                </div>
                                <div id="dopAdress" class="blocki">
                                    <a href="#"><img src="img/akk/hello/Vector1.svg" alt="">
                                        <div class="blocki-txt">Мои адреса</div>
                                    </a>
                                </div>
                            </div>
                            <div class="line">
                                <div id="dopRed" class="blocki">
                                    <a href="#"><img src="img/akk/hello/Vector2.svg" alt="">
                                        <div class="blocki-txt">Редактировать профиль</div>
                                    </a>
                                </div>
                                <div class="blocki">
                                    <a href="#"><img src="img/akk/hello/Vector3.svg" alt="">
                                        <div class="blocki-txt">Избранные товары</div>
                                    </a>
                                </div>
                                <div class="blocki">
                                    <a href="{{route('user.logout')}}"><img src="img/akk/hello/Vector4.svg" alt="">
                                        <div class="blocki-txt">Выход</div>
                                    </a>
                                </div>
                            </div>
                        </div>

                        <div class="container-right1">
                            <form action="{{route('user.update', ['id' => auth()->user()->id])}}" method="POST">
                                @csrf
                                <div class="blockis">
                                    <div class="top">
                                        <div class="name">
                                            <div class="custom-field">
                                                <label>Ваше имя:</label>
                                                <input name="name" value="{{auth()->user()->name}}" id="name-field"
                                                       type="text" placeholder="Name"/>
                                                @error('name')
                                                <p style="color: red">{{$message}}</p>
                                                @enderror

                                            </div>
                                        </div>
                                        <div class="ferstname">
                                            <div class="custom-field">
                                                <label>Ваша фамилия:</label>
                                                <input id="last-field" name="first_name"
                                                       value="{{auth()->user()->first_name}}" type="text"
                                                       placeholder="Введите вашу фамилию"/>
                                                @error('first_name')
                                                <p style="color: red">{{$message}}</p>
                                                @enderror

                                            </div>
                                        </div>
                                    </div>
                                    <div class="bot">
                                        <div class="email">
                                            <div class="custom-field">
                                                <label>Email адрес:</label>
                                                <input id="email-field" name="email" value="{{auth()->user()->email}}"
                                                       type="email" placeholder="vladstrok23@gmail.com"/>
                                                @error('email')
                                                <p style="color: red">{{$message}}</p>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="email">
                                            <div class="custom-field">
                                                <label for="">Номер телефона:</label>
                                                <input id="" value="{{auth()->user()->phone}}" type="phone" name="phone" placeholder="+375445656947"/>
                                                @error('phone')
                                                <p style="color: red">{{$message}}</p>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="button-vhode">
                                        <div class="katalogh">
                                            <button class="save-users" type="submit"> Сохранить </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="histiri-zakaz">
                            <div class="header-zakaz">
                                <span class="h-zak">Модель товара</span>
                                <span class="h-zak">Дата</span>
                                <span class="h-zak">Статутс</span>
                                <span class="h-zak">Итог</span>
                            </div>
                            <div class="zakazs">
                                @foreach($orders as $order)
                                    <div class="container-zakaz">
                                        <span class="txtt numder">{{ \App\Models\Product::find($order->product_id)->model }}</span>
                                        <span class="txtt data">{{$order->time}}</span>
                                        <span class="txtt status">{{ \App\Enum\Order\StatusEnum::tryFrom($order->status)->lable() }}</span>
                                        <span class="txtt itog">{{ $order->sum }} руб.</span>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="adressa">
                            <div class="container-adresa">

                                @foreach($adress as $adres)
                                    <div class="block-adres">
                                        <div
                                            class="name-pokup">{{ \Illuminate\Support\Facades\Auth::user()->name ?? null }}</div>
                                        <div
                                            class="adres">{{ $adres->country . '. ' . $adres->city . '. ' . $adres->house . '. '. $adres->room }}</div>
                                        <div class="email-adres">
                                            <label>Email:</label>
                                            <span>{{\Illuminate\Support\Facades\Auth::user()->email}}</span>
                                        </div>
                                        <div class="blok-adres-uprav">
{{--                                            <button id="redModalBtn"  class="redd-adres">Редактировать</button>--}}
                                            <form action="{{route('user.address.delete', ['id' => $adres->id])}}" method="post">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="delete-adres">Удалить</button>
                                            </form>
                                            <div class="nomer-adresa">адрес</div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div id="myModal" class="modal">
                                <div class="modal-content">
                                    <span class="close">&times;</span>
                                    <h2>Редактировать адрес</h2>
                                    <form id="addressForm">
                                        <label for="address">Name:</label>
                                        <input class="adresvvod" type="text" id="name" name="address">
                                        <label for="address">Адрес:</label>
                                        <input class="adresvvod" type="text" id="address" name="address">
                                        <label for="address">Email:</label>
                                        <input class="adresvvod" type="text" id="email" name="address">
                                        <button class="save-users" type="submit">Сохранить</button>
                                    </form>
                                </div>
                            </div>
                            <div id="myModaladd" class="modaladd">
                                <div class="modal-content">
                                    <span class="closeAdres">&times;</span>
                                    <h2>Добавить адрес</h2>
                                    <form id="addressForm" method="post" action="{{route('user.add-adress')}}">
                                         @csrf
                                        <label for="address">Город:</label>
                                        <input class="adresvvod" type="text" id="name" name="country">
                                        <label for="address">Улица:</label>
                                        <input class="adresvvod" type="text" id="address" name="city">
                                        <label for="address">Дом:</label>
                                        <input class="adresvvod" type="text" id="email" name="house">
                                        <label for="address">Квартира:</label>
                                        <input class="adresvvod" type="text" id="email" name="room">
                                        <button class="save-users" type="submit">Сохранить</button>
                                    </form>
                                </div>
                            </div>
                            <button id="addModalBtn" class="add-adres">Добавить новый адрес</button>
                        </div>
                        <div class="otzv">
                            <div class="review-form">
                                <form id="reviewForm" method="post" action="{{ route('user.review.store') }}">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Имя:</label>
                                        <input type="text" id="name" name="name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="date">Дата:</label>
                                        <input type="date" id="date" name="date" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="review">Отзыв:</label>
                                        <textarea id="review" name="description" class="form-control" rows="4" required></textarea>
                                    </div>
                                    <button type="submit" class="submit-btn">Отправить отзыв</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
