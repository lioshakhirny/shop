@extends('layouts.site.main')
@section('content')
    <div class="container">
        <form class="formss" method="get" action="{{route('catalog.index')}}">
            @csrf
            <div id="filters" class="left">
                <div class="ccontainer">
                    <div class="filter">
                        <div class="ttxt">
                            <div class="glav-txt">Фильтр по цене</div>
                            <div class="podh-txt">^</div>
                        </div>
                        <div class="price-input">
                            <div class="field">
                                <span>Min</span>
                                <input name="min" type="number" class="input-min" value="30">
                            </div>
                            <div class="separator">-</div>
                            <div class="field">
                                <span>Max</span>
                                <input name="max" type="number" class="input-max" value="1000">
                            </div>
                        </div>
                        <div class="slider">
                            <div class="progress"></div>
                        </div>
                        <div class="range-input">
                            <input type="range" class="range-min" min="0" max="1000" value="30" step="50">
                            <input type="range" class="range-max" min="0" max="1000" value="1000" step="50">
                        </div>
                    </div>
                </div>
                <div class="ccontainer">
                    <div class="size">
                        <div class="ttxtx">
                            <div class="glav-txt">Размер(EU)</div>
                            <div class="podh-txt">^</div>
                        </div>
                        <div class="soderh">
                            <div class="linne">
                                @foreach($sizes as $size)
                                    <div class="size-container">
                                        <div class="sod-size" onclick="changeSize(this)">{{$size->size}}</div>
                                        <input type="checkbox" name="sizes[]" value="{{ $size->size }}" style="display: none;">
                                    </div>
                                @endforeach
                            </div>

                        </div>
                    </div>
                </div>
                <div class="ccontainer">

                    <div class="brend">
                        <div class="ttxtx">
                            <div class="glav-txt">Модель</div>
                            <div class="podh-txt">^</div>
                        </div>
                        @foreach($products as $product)
                            <div class="chek-brend">
                                <input class="custom-checkbox" id="myCheckbox" name="product_id[]" value="{{$product->id}}"
                                       type="checkbox">
                                <label for="myCheckbox">
                                    {{$product->model}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="ccontainer">

                    <div class="brend">
                        <div class="ttxtx">
                            <div class="glav-txt">Брэнд</div>
                            <div class="podh-txt">^</div>
                        </div>
                        @foreach($products as $product)
                            <div class="chek-brend">
                                <input class="custom-checkbox" id="myCheckbox" name="product_id[]" value="{{$product->id}}"
                                       type="checkbox">
                                <label for="myCheckbox">
                                    {{$product->brand}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="ccontainer">
                    <div class="size">
                        <div class="ttxtx">
                            <div class="glav-txt">Цвет</div>
                            <div class="podh-txt">^</div>
                        </div>
                        <div class="soderh">

                                <div class="line-color">
                                    <input type="checkbox" id="color-white" name="color[]" value="white">
                                    <div class="color white" onclick="color(this)" data-checkbox-id="color-white"></div>
                                    <div class="namess">Белый</div>
                                </div>
                                <div class="line-color">
                                    <input type="checkbox" id="color-blue" name="color[]" value="blue">
                                    <div class="color blue" onclick="color(this)" data-checkbox-id="color-blue"></div>
                                    <div class="namess">Синий</div>
                                </div>
                                <div class="line-color">
                                    <input type="checkbox" id="color-red" name="color[]" value="red">
                                    <div class="color red" onclick="color(this)" data-checkbox-id="color-red"></div>
                                    <div class="namess">Красный</div>
                                </div>


                                <div class="line-color">
                                    <input type="checkbox" id="color-yellow" name="color[]" value="yellow">
                                    <div class="color yellow" onclick="color(this)" data-checkbox-id="color-yellow"></div>
                                    <div class="namess">Желтый</div>
                                </div>
                                <div class="line-color">
                                    <input type="checkbox" id="color-orange" name="color[]" value="orange">
                                    <div class="color orange" onclick="color(this)" data-checkbox-id="color-orange"></div>
                                    <div class="namess">Оранжевый</div>
                                </div>



                                <div class="line-color">
                                    <input type="checkbox" id="color-pink" name="color[]" value="pink">
                                    <div class="color pink" onclick="color(this)" data-checkbox-id="color-pink"></div>
                                    <div class="namess">Розовый</div>
                                </div>
                                <div class="line-color">
                                    <input type="checkbox" id="color-purple" name="color[]" value="purple">
                                    <div class="color purple" onclick="color(this)" data-checkbox-id="color-purple"></div>
                                    <div class="namess">Фиолетовый</div>
                                </div>
                                <div class="line-color">
                                    <input type="checkbox" id="color-black" name="color[]" value="black">
                                    <div class="color black" onclick="color(this)" data-checkbox-id="color-black"></div>
                                    <div class="namess">Черный</div>
                                </div>

                        </div>


                    </div>
                </div>
                <div class="ccontainer">
                    <button type="submit" class="btn btn-primary">Отфильтровать</button>
                    <button class="btn-primary" type="submit">Сбросить все фильтры</button>
                </div>
            </div>
        </form>
        <div class="right">
            <form class="poisk" action="{{route('catalog.index')}}" method="get">
                @csrf
                <div class="search-container">
                    <input name="model" type="text" class="search-input" placeholder="Поиск...">
                    <button type="submit" class="search-btn">
                        <svg class="search-icon" viewBox="0 0 24 24">
                            <path d="M10,17C7.243,17,5,14.757,5,12s2.243-5,5-5s5,2.243,5,5S12.757,17,10,17z M21.707,20.293l-5.292-5.292
            C17.686,13.878,18,12.958,18,12c0-4.418-3.582-8-8-8S2,7.582,2,12s3.582,8,8,8c0.958,0,1.878-0.314,2.707-0.879l5.292,5.292
            C18.488,23.902,18.744,24,19,24s0.512-0.098,0.707-0.293C21.098,23.512,21.098,21.902,21.707,20.293z"/>
                        </svg>
                    </button>
                </div>
            </form>
            <div class="txt">
                <div class="txt-shekear">Обувь</div>
                <div class="open-filter" onclick="toggleFilters()">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                        <path fill="currentColor" d="M22 18.605a.75.75 0 0 1-.75.75h-5.1a2.93 2.93 0 0 1-5.66 0H2.75a.75.75 0 1 1 0-1.5h7.74a2.93 2.93 0 0 1 5.66 0h5.1a.75.75 0 0 1 .75.75m0-13.21a.75.75 0 0 1-.75.75H18.8a2.93 2.93 0 0 1-5.66 0H2.75a.75.75 0 1 1 0-1.5h10.39a2.93 2.93 0 0 1 5.66 0h2.45a.74.74 0 0 1 .75.75m0 6.6a.74.74 0 0 1-.75.75H9.55a2.93 2.93 0 0 1-5.66 0H2.75a.75.75 0 1 1 0-1.5h1.14a2.93 2.93 0 0 1 5.66 0h11.7a.75.75 0 0 1 .75.75" />
                    </svg>
                </div>
                <div class="txt-sort">
                    <div class="rear">Сортировать по - </div>
                    <form class="sotrsts" action="{{route('catalog.index')}}" method="get">
                        @csrf
                        <div class="front"> От дешевых к дорогим</div>
                        <input type="hidden" value="sort" name="sort">
                        <button class="sotss" type="submit" value="Сортировать">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                                <path fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5" d="M10 14H2m6-4H2m4-4H2m10 12H2m17 2V4m0 16l3-3m-3 3l-3-3m3-13l3 3m-3-3l-3 3" />
                            </svg>
                        </button>
                    </form>
                </div>
            </div>
            <div class="line">
                @foreach($products as $product)
                    @if($product->count > 0)
                        <div class="tovar">
                            <a href="{{route('catalog.show', ['id' => $product->id])}}">
                                <img
                                    src="{{asset( Storage::url(Arr::first(Storage::allFiles('public/files/products/' . $product->id))))}}"
                                    alt=""
                                    width="200"
                                    height="200"
                                >
                                <div class="name">{{$product->model}}</div>
                                <div class="price">
                                    <div class="prices">{{$product->price}}</div>
                                    <div class="valuta">BYN</div>
                                </div>
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection
<script>
    function toggleFilters() {
        var filters = document.getElementById('filters');
        if (filters.classList.contains('filters-hidden')) {
            filters.classList.remove('filters-hidden');
            filters.classList.add('filters-visible');
        } else {
            filters.classList.remove('filters-visible');
            filters.classList.add('filters-hidden');
        }
    }

</script>
