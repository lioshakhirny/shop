@extends('layouts.site.main')
@section('content')
    <div class="container">
        <div class="block">
            <div class="leftt">
                <div class="img-glav">
                    <img id="glav" src="{{$image_one}}" alt="">
                    <div class="block-img">
                            <div class="sideways"><img id="sideeways" width="300" src="{{$image_one}}" alt="">
                            </div>
                            <div class="up"><img width="300" height="300" id="up" src="{{$image_two}}" alt=""></div>
                            <div class="lap"><img width="300" height="300" id="lap" src="{{$image_three}}" alt=""></div>
                    </div>
                </div>
            </div>
            <div class="right">
                <div class="name">Кроссовки {{$product->model}}</div>
                <div class="txt-size">EU размеры:</div>
                <div class="soderh">
                    <div class="linne">
                        @foreach($sizes as $size)
                            <div class="sod-size" onclick="changeColor({{$size->size}})">{{$size->size}}</div>
                        @endforeach
                    </div>
                </div>
                <div class="container-size">
                    <div class="size">размер - </div>
                    <div class="button-vhode">
                        <div id="add" class="katalog">
                            <a href="">Добавить в корзину</a>
                            <input type="hidden" value="{{route('busket.add-product')}}" class="route">
                            <input type="hidden" value="{{$product->id}}" class="product_id">
                            @auth
                                <input type="hidden" value="{{auth()->user()->id}}" class="user_id">
                            @else
                                <input type="hidden" value="null" class="user_id">
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="detali">
            <div class="line-container">
                <div class="line">
                    <div class="txt-line-detali">Детали</div>
                    <div class="txt-line-opisania">Описание</div>
                </div>
            </div>
            <div class="container-detali">
                <div class="det">
                    <div class="det-txt">ID</div>
                    <div class="det-line"></div>
                    <div class="det-name">{{$product->id}}</div>
                </div>
                <div class="det">
                    <div class="det-txt">Категория</div>
                    <div class="det-line"></div>
                    <div class="det-name">Обувь</div>
                </div>
                <div class="det">
                    <div class="det-txt">Бренд</div>
                    <div class="det-line"></div>
                    <div class="det-name">{{$product->brand}}</div>
                </div>
                <div class="det">
                    <div class="det-txt">Модель</div>
                    <div class="det-line"></div>
                    <div class="det-name">{{$product->model}}</div>
                </div>
                <div class="det-colorss">
                    <div class="det-txt">Цвет</div>
                    <div class="det-line"></div>
                    <div class="det-name"><div class="color-block"></div></div>
                </div>
                <div class="det">
                    <div class="det-txt">Цена(р)</div>
                    <div class="det-line"></div>
                    <div id="price" class="det-name">{{$product->price}}</div>
                </div>
                <div class="det">
                    <div class="det-txt">Количество на складе</div>
                    <div class="det-line"></div>
                    <div id="kolvo-sklad" class="det-name">{{$product->count}}</div>
                </div>
                <div class="det-promocod">
                    <div class="det-txt">Промокод</div>
                    <div class="det-line"></div>
                    <div class="det-name">
                        <input type="text" id="kolvo" placeholder="Введите промокод" >
                    </div>
                </div>
                <div class="det">
                    <div class="det-txt">Итоговая цена</div>
                    <div class="det-line"></div>
                    <div id="ob-summa" class="det-name">110</div>
                </div>
            </div>
            <div class="container-opisanie">
                <div class="text-opisania">{{$product->description}}</div>
            </div>
            .

        </div>
        <div class="predloh">
            <div class="txt-predloh">Интересные предложения</div>
            <div class="container-predloh">
                <div class="lines">
                    @foreach($products as $product)
                    <div class="tovar">
                        <a href="{{route('catalog.show', ['id' => $product->id])}}"><img src="{{Storage::url(Arr::first(Storage::allFiles('public/files/products/' . $product->id)))}}" alt="">
                            <div class="name">{{ $product->model }}</div>
                            <div class="price">
                                <div class="ot">от</div>
                                <div class="prices">{{$product->price}}</div>
                                <div class="valuta">BYN</div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>

        </div>
    </div>
@endsection
