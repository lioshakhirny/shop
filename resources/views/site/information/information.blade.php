@extends('layouts.site.main')
@section('content')
    <div class="wraper">
            <div class="container">
                <h2>О интернет магазине Sneaker Haven</h2>
                <p>Добро пожаловать в Sneaker Haven – ваш источник подлинных кроссовок и непревзойденного стиля! Мы рады
                    представить вам уникальную онлайн-платформу, где вы сможете окунуться в мир инноваций и моды от
                    легендарного бренда спортивной обуви.</p>
                <div class="image-opisanie">
                    <img src="{{asset('assets/image/opisanie/image.png.png')}}">
                </div>
                <p> Мы предлагаем широкий выбор обуви от лучших брендов, чтобы удовлетворить ваши потребности в уличной
                    моде. Наш приоритет - ваше удовлетворение, поэтому мы уделяем внимание каждой детали, чтобы сделать
                    вашу покупку простой и приятной. Присоединяйтесь к нашему сообществу стильных обувных ценителей и
                    обновите свой гардероб с помощью Sneaker Haven уже сегодня!</p>
                <h1>Эстэтика наших кроссовок</h1>
                <div class="block">
                    <p>Наши кроссовки отличаются не только эстетическим великолепием, но и функциональностью. Мы
                        предлагаем широкий выбор моделей для разных видов спорта, от бега до баскетбола, и для различных
                        повседневных ситуаций. Будь то икона стиля, такая как Air Max, или универсальная классика, как
                        Air Force 1 - у нас есть именно то, что подойдет вам.
                        На SwooshStore мы ценим ваше доверие и Storageкомфортность при покупке. Вся наша обувь поставляется
                        непосредственно от производителя, что гарантирует вам аутентичность каждой пары кроссовок. Мы
                        также предлагаем удобные опции доставки и безопасные методы оплаты, чтобы сделать ваш опыт
                        покупки максимально приятным и беззаботным.
                        Присоединяйтесь к нашему сообществу любителей Nike, чтобы разделить радость от качественной
                        обуви и уникального стиля. Мы всегда готовы помочь вам с выбором, ответить на вопросы и
                        обеспечить вас идеальной парой кроссовок, которая подчеркнет вашу индивидуальность и даст вам
                        уверенность в каждом шаге.
                        Спасибо, что выбираете SwooshStore – ваш источник оригинальных кроссовок Nike!</p>
                    <div class="block-img">
                        <img src="{{asset('assets/image/opisanie/Group%2017.png')}}">
                    </div>
                </div>
                <div class="container-garant">
                    <div class="block-garant">
                        <div class="block-garant-img"><img src="{{asset('assets/image/opisanie/hit.svg')}}"></div>
                        <div class="block-garant-txt">
                            <div class="glav-garant-txt">Только оригинальные товары</div>
                            <div class="podh-garant-txt">Гарантированная подлинность и высокое качество кроссовок.</div>
                        </div>
                    </div>
                    <div class="block-garant">
                        <div class="block-garant-img"><img src="{{asset('assets/image/opisanie/zvezd.svg')}}"></div>
                        <div class="block-garant-txt">
                            <div class="glav-garant-txt">Профессиональный сервис</div>
                            <div class="podh-garant-txt">Команда экспертов, готовых помочь с выбором размера ответить на
                                все вопросы.
                            </div>
                        </div>
                    </div>
                    <div class="block-garant">
                        <div class="block-garant-img"><img src="{{asset('assets/image/opisanie/korzina.svg')}}"></div>
                        <div class="block-garant-txt">
                            <div class="glav-garant-txt">Эксклюзивный выбор</div>
                            <div class="podh-garant-txt">Богатый ассортимент оригинальных моделей Nike, включая редкие
                                выпуски.
                            </div>
                        </div>
                    </div>
                </div>
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2769.1820578291104!2d25.30256989809848!3d53.077357658384805!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2s!4v1712671401496!5m2!1sru!2s"
                    width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"
                    referrerpolicy="no-referrer-when-downgrade"></iframe>
                <div class="dataa">
                    <div class="adres">
                        <div class="data-h">Адрес:</div>
                        <div class="data-p">г.Слоним пр-т Независимости д. 23</div>
                    </div>
                    <div class="telefon">
                        <div class="data-h">Телефон:</div>
                        <div class="data-p">+375(44) 5656-947</div>
                    </div>
                    <div class="email">
                        <div class="data-h">Почта:</div>
                        <div class="data-p">vladstrok23@gmail.com</div>
                    </div>
                </div>
            </div>
    </div>
@endsection
