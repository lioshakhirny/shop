@extends('layouts.site.main')
@section('content')
<div class="wraper">
    <main>
        <section class="blog-posts">
            @foreach($blogs as $blog)
                <article class="post">
                    <h2>{{ $blog->title }}</h2>
                    <p class="date">{{$blog->created_at}}</p>
                    <p class="description">{{$blog->description}}</p>
                    <div class="button-vhode">
                        <div class="kataloggg">
                            <a href="{{route('catalog.index')}}">В каталог</a>
                        </div>
                    </div>
                </article>
            @endforeach
        </section>
    </main>
</div>
@endsection
