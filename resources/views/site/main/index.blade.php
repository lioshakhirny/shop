@extends('layouts.site.main')
@section('content')
<div class="container">
    <section>
        <div class="nahalo">
            <div class="podbor">
                <span class="lal">ПОДБЕРИ СЕБЕ СВОИ <br> </span>
                <span class="lol">КрОССОВКИ</span>
            </div>
            <div class="nadpis">Обувь от известные брендов у нас в каталоге. <br>Только качественная обувь.</div>
            <div class="katalog">
                <a href="{{route('catalog.index')}}">Перейти в каталог ></a>
            </div>
        </div>
        <div class="container-garant">
            <div class="block-garant">
                <div class="block-garant-img"><img src="{{asset('assets/image/opisanie/hit.svg')}}"></div>
                <div class="block-garant-txt">
                    <div class="glav-garant-txt">Только оригинальные товары</div>
                    <div class="podh-garant-txt">Гарантированная подлинность и высокое качество кроссовок.</div>
                </div>
            </div>
            <div class="block-garant">
                <div class="block-garant-img"><img src="{{asset('assets/image/opisanie/zvezd.svg')}}"></div>
                <div class="block-garant-txt">
                    <div class="glav-garant-txt">Профессиональный сервис</div>
                    <div class="podh-garant-txt">Команда экспертов, готовых помочь с выбором размера ответить на все вопросы.</div>
                </div>
            </div>
            <div class="block-garant">
                <div class="block-garant-img"><img src="{{asset('assets/image/opisanie/korzina.svg')}}"></div>
                <div class="block-garant-txt">
                    <div class="glav-garant-txt">Эксклюзивный выбор</div>
                    <div class="podh-garant-txt">Богатый ассортимент оригинальных моделей Nike, включая редкие выпуски.</div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="sneakers">
            <div class="text-1">Обувь</div>
            <div class="text-2">
                <a href="{{route('catalog.index')}}">Больше товаров ></a>
            </div>
        </div>
        <div class="tovars">
            @foreach($products as $product)
            <div class="tovar-1">
                <a href="{{route('catalog.show', ['id' => $product->id])}}">
                    <img src="{{Storage::url(Arr::first(Storage::allFiles('public/files/products/' . $product->id)))}}" alt=""></a>
                <div class="name">{{$product->brand . ' ' . $product->model }}</div>
                <div class="price">от {{$product->price . ' BYN'}}</div>
            </div>
            @endforeach
        </div>
    </section>
    <section>
        <div class="sneakers">
            <div class="text-1">Лучшее</div>
            <div class="text-2"><a href="{{route('catalog.index')}}">Больше товаров ></a></div>
        </div>
        <div class="tovars">
            @foreach($products as $product)
                <div class="tovar-1">
                    <a href="{{route('catalog.show', ['id' => $product->id])}}">
                        <img src="{{Storage::url(Arr::first(Storage::allFiles('public/files/products/' . $product->id)))}}" alt=""></a>


                    <div class="name">{{$product->brand . ' ' . $product->model }}</div>
                    <div class="price">от {{$product->price . ' BYN'}}</div>
                </div>
            @endforeach
        </div>
    </section>
    <section>
        <div class="containerr">
            <button id="myBtn">Наверх</button>

            <div class="stoimost">
                <div class="rashot">
                    <div class="texts">
                        <div class="txt-1">Новый привоз обуви с Европы</div>

                        <div class="txt-2">Были налажены отношения с постовщиками оригинальной обуви с Европы, в каталоге будут присутствовать оригинальная обувь от известных брэндов таких как: Nike, Adidas,Pumma и тд.</div>
                        <div class="txt-3">
                            <div class="txt-3-1">
                                <div class="number">
                                    1
                                </div>
                                <div class="ll">Только оригинальная обувь, от качественных производителей!</div>
                            </div>
                            <div class="txt-3-2">
                                <div class="number">
                                    2
                                </div>
                                <div class="ll">Доступны многие размеры и  модели обуви, которые вам точно понравятся</div>
                            </div>
                        </div>
                        <div class="txt-4">
                            <div class="katalog">
                                <a href="{{route('catalog.index')}}">Перейти в каталог ></a>
                            </div>
                        </div>
                    </div>
                    <img src="{{asset('assets/image/snekers/Group 329.png')}}" alt=""></div>

            </div>

        </div>
        <div class="container-otzv">
            <h1 class="otz-txt">Отзывы наших поситителей</h1>
            <div class="reviews-wrapper">
                <button class="prev">&#10094;</button>
                <div class="reviews-inner">
                    @foreach($reviews as $review)
                    <div class="review active">
                        <div class="content">
                            <div class="author">{{$review->name}}</div>
                            <div class="date">{{$review->created_at}}</div>
                            <p>{{ $review->description }}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                <button class="next">&#10095;</button>
            </div>
        </div>
        <div class="teams">
            <div class="team">
                <div class="txxt">
                    <div class="q">О ИНТЕРНЕТ МАГАЗИНЕ Sneaker Haven</div>
                    <div class="w">Команда Sneaker Haven предоставляет услугу доставки только оригинальных товаров c крупнейшего китайского маркетплейса Poizon, чтобы наши клиенты экономили более 40% на каждой покупке. Работаем без посредников, благодаря
                        чему можем предоставлять лучшую цену. Быстрая, бесплатная доставка. Сайт, на котором можно будет удобно оформить покупку, не скачивая китайское мобильное приложение Poizon, с удобной фильтрацией огромного количества
                        товаров, а так же с возможностью сразу увидеть окончательную цену товара.</div>
                </div>
                <div class="fram">
                    <div class="containnner">
                        <div class="dost">
                            <div class="izbr"><img src="{{asset('assets/image/snekers/dost.svg')}}" alt=""></div>
                            <div class="glav">
                                <div class="glav-txt">Бесплатная доставка до Беларуси</div>
                                <div class="podh-txt">Доставим вам заказ абсолютно бесплатно до Беларуси</div>
                            </div>

                        </div>
                        <div class="dostt">
                            <div class="izbr"><img src="{{asset('assets/image/snekers/posred.svg')}}" alt=""></div>
                            <div class="glav">
                                <div class="glav-txt">мы Работаем без посредников</div>
                                <div class="podh-txt">Между нами и клиентом нет третьего лишнего</div>
                            </div>

                        </div>
                        <div class="dos">
                            <div class="izbr"><img src="{{asset('assets/image/snekers/zakaz.svg')}}" alt=""></div>
                            <div class="glav">
                                <div class="glav-txt">простота в заказе и использовании</div>
                                <div class="podh-txt">Для заказа с Poizon не нужно никаких приложений</div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="blog">
            <div class="sneakers">
                <div class="text-1">Наш блог</div>
                <div class="text-2">Больше статей ></div>
            </div>
            <div class="blogers">
                <div class="ris">
                    <img src="{{asset('assets/image/snekers/saske 1.svg')}}" alt="">
                    <div class="blog-glav-txt">Делаем уникальный окрас твоих кросовок</div>
                    <div class="blog-podh-txt">Мы связываемся с художниками которые умеют хорошо рисовать.</div>
                </div>
                <div class="sport"><img src="{{asset('assets/image/snekers/bytz 1.svg')}}" alt="">
                    <div class="blog-glav-txt">В продаже имеются спортивные бутсы, бампы и тд.</div>
                    <div class="blog-podh-txt">Нам поставляют компании такие как nike, adidas оригинальную спортивную обувь.</div>
                </div>
                <div class=" diz"><img src="{{asset('assets/image/snekers/diz 1.svg')}}" alt="">
                    <div class="blog-glav-txt">У нас в наличии есть дизайнерская обувь</div>
                    <div class="blog-podh-txt">Дизайнерская обувь, от знаменитых ютьюберов.</div>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection
