@extends('layouts.site.main')
@section('content')
    <div class="container">
        @if(empty($carts))
            <div class="block">
                <div class="galv-txt">Корзина товаров</div>
                <div class="glav-block">
                    <div class="block-img"><img src="{{asset('assets/image/karzina/Group 369.svg')}}" alt=""></div>
                    <div class="block-gtxt">Ваша корзина на данный момент пуста.</div>
                    <div class="block-ptxt">Прежде чем приступить к оформлению заказа, вы должны добавить некоторые
                        товары в корзину. На странице "Каталог" вы найдете много интересных товаров.
                    </div>
                    <div class="button-vhode">
                        <div class="katalog">
                            <a href="{{route('catalog.index')}}">Вернуться в каталог ></a>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="shopping-cart">
                <!-- Title -->
                <div class="title">
                    Корзина
                </div>
                @foreach($carts as $cart)
                    <div class="item" data-product-id="{{ $cart['product_id'] }}">
                        <form action="{{ route('busket.delete-order') }}" method="post">
                            @csrf
                            <div class="buttonsss">
                                <input type="hidden" name="product_id" value="{{ $cart['product_id'] }}">
                                <button type="submit" class="delete-btn"></button>
                            </div>
                        </form>
                        <div class="image">
                            <img width="100" src="{{\Illuminate\Support\Facades\Storage::url(\Illuminate\Support\Arr::first(Storage::allFiles('public/files/products/' . $cart['product_id'])))}}" alt=""/>
                        </div>
                        <div class="description">
                            <span>{{$cart['brand']}}</span>
                            <span>{{$cart['model']}}</span>
                        </div>
                        <div class="quantity">
                            <button class="plus-btn" type="button" name="button" data-action="plus">
                                <img src="{{asset('assets/image/plus.svg')}}" alt=""/>
                            </button>
                            <input type="text" name="quantity" value="{{ $cart['count'] }}" data-quantity>
                            <button class="minus-btn" type="button" name="button" data-action="minus">
                                <img src="{{asset('assets/image/minus.svg')}}" alt=""/>
                            </button>
                        </div>
                        <div class="total-price" data-price>{{$cart['price']}} BYN</div>
                    </div>
                @endforeach
                <button class="checkout-btn">Оформить заказ</button>
            </div>
            <div id="custom-modal" class="custom-modal">
                <div class="custom-modal-content">
                    <span class="custom-close">&times;</span>
                    <h2>Оформление заказа</h2>
                    <form method="post" action="{{route('busket.add-order')}}">
                        @csrf
                        <p id="custom-total-amount">Общая сумма: <span>{{array_sum(array_column($carts, 'price'))}}</span> руб.</p>
                        <p id="custom-order-time">Время оформления: <span></span></p>
                        <label for="custom-promo-code">Промокод:</label>
                        <input type="text" id="custom-promo-code" name="code">
                        <label for="custom-payment-method">Выбор оплаты:</label>
                        <select id="custom-payment-method" name="custom-payment-method">
                            <option value="cash">Наличными</option>
                        </select>
                        <label for="custom-payment-method">Выбор адреса доставки:</label>
                        <select id="custom-payment-method" name="adress_id">
                            @foreach($adreses as $adres)
                                <option
                                    value="{{$adres->id}}">{{ $adres->country . '. ' . $adres->city . '. ' . $adres->house . '. '. $adres->room }}</option>
                            @endforeach
                        </select>
                        <button id="custom-confirm-order" type="submit">Подтвердить заказ</button>
                    </form>
                </div>
            </div>

        @endif
    </div>
@endsection
<script>
    document.addEventListener('DOMContentLoaded', (event) => {
        const items = document.querySelectorAll('.item');

        items.forEach(item => {
            const plusBtn = item.querySelector('[data-action="plus"]');
            const minusBtn = item.querySelector('[data-action="minus"]');
            const quantityInput = item.querySelector('[data-quantity]');
            const totalPriceElem = item.querySelector('[data-price]');
            const pricePerItem = parseFloat(totalPriceElem.textContent) / parseInt(quantityInput.value);

            function updateTotalPrice() {
                const quantity = parseInt(quantityInput.value);
                const totalPrice = (quantity * pricePerItem).toFixed(2);
                totalPriceElem.textContent = `${totalPrice} BYN`;
            }

            plusBtn.addEventListener('click', () => {
                let quantity = parseInt(quantityInput.value);
                quantityInput.value = ++quantity;
                updateTotalPrice();
            });

            minusBtn.addEventListener('click', () => {
                let quantity = parseInt(quantityInput.value);
                if (quantity > 1) {
                    quantityInput.value = --quantity;
                    updateTotalPrice();
                }
            });

            updateTotalPrice();  // Initial update for correct total price on page load
        });
    });


</script>
