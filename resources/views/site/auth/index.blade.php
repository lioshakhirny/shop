@extends('layouts.site.main')
@section('content')
    <div class="container" xmlns="http://www.w3.org/1999/html">
        <div class="akk">Аккаунт</div>
        <div class="block">
            <div class="vhode">
                <form method="post" action="{{route('login.store')}}">
                    @csrf
                    <div class="otstyp">
                        <div class="txt-glav">Войти</div>
                        <div class="email">
                            <div class="custom-field">
                                <label for="">Email адрес:</label>
                                <input id="email-field" name="email" type="email" placeholder="Enter Email" value="{{ old('email') }}" />
                                @error('email')
                                <p style="color: red">{{$message}}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="pasword">
                            <div class="custom-field">
                                <label for="">Пароль:</label>
                                <input id="password-field" name="password" type="password" placeholder="Password" />
                                @error('password')
                                <p style="color: red">{{$message}}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="zapom">
                            <div class="zp">Забыли пароль:</div>
                            <div class="zab">Напишите в поддержку.</div>
                        </div>
                        <div class="button-vhode">
                            <div class="katalog">
                                <button type="submit">Войти</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="registr">
                <form method="post" action="{{route('register.store')}}">
                    @csrf
                    <div class="otstyp">
                        <div class="txt-glav">Регистрация</div>
                        <div class="custom-field">
                            <label for="">Имя: </label>
                            <input id="email-field" name="name" type="text" placeholder="Имя"/>
                            @error('name')
                            <p style="color: red">{{$message}}</p>
                            @enderror
                        </div>
                        <div class="email">
                            <div class="custom-field">
                                <label for="">Email адрес:</label>
                                <input id="email-field" name="email" type="email" placeholder="Enter Email"/>
                                @error('email')
                                <p style="color: red">{{$message}}</p>
                                @enderror
                            </div>
                        </div>
                        <div class="pasword">
                            <div class="custom-field">
                                <label for="">Пароль:</label>
                                <input id="email-field" name="password" type="password" placeholder="Password"/>
                                @error('password')
                                <p style="color: red">{{$message}}</p>
                                @enderror
                            </div>
                        </div>

                        <div class="custom-field">
                            <label for="">Повторите пароль:</label>
                            <input id="email-field" name="reset_password" type="password" placeholder="Password"/>
                        </div>

                        <div class="button-vhode">
                            <div class="katalogh">
                                <button type="submit">Регистрация</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if ($errors->has('email'))
            <div class="block-errors">
                <div class="error-block">
                    Неправильный email или пароль
                </div>
            </div>
        @endif

    </div>
@endsection
